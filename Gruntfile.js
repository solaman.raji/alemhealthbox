module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
            dashboard_js: {
                src: [


                    'static/bower_components/jquery/dist/jquery.min.js',
                    'static/bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'static/bower_components/angular/angular.min.js',
                    'static/bower_components/angular-resource/angular-resource.min.js',
                    'static/bower_components/angular-ui-router/release/angular-ui-router.js',
                    'static/bower_components/angular-cookies/angular-cookies.min.js',
                    'static/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'static/bower_components/angular-flash/dist/angular-flash.min.js',

                    'static/bower_components/angular-google-chart/ng-google-chart.js',
                    'static/bower_components/textAngular/dist/textAngular-rangy.min.js',
                    'static/bower_components/textAngular/dist/textAngular-sanitize.min.js',
                    'static/bower_components/textAngular/dist/textAngular.min.js',

                    'static/client/app/app.js',
                    'static/client/app/modules/*/app.js',
                    'static/client/app/modules/*/*.js',
                    'static/client/app/common/*/*.js'


                ],
                dest: 'static/assets/build/script.js'
            },
            dashboard_css: {
               src: [
                    'static/bower_components/bootstrap/dist/css/bootstrap.min.css',
                    'static/bower_components/font-awesome/css/font-awesome.min.css',
                    'static/dashboard/css/ah-admin.css',
                    'static/bower_components/textAngular/src/textAngular.css'

               ],
               dest: 'static/assets/build/style.css'
            }


        },
        watch: {
            files: [
                    'static/client/app/app.js',
                    'static/client/app/modules/*/app.js',
                    'static/client/app/modules/*/*.js',
                    'static/client/app/common/*/*.js',
                    'static/client/app/libs/*.js',
                    'static/dashboard/css/ah-admin.css',
                    'static/new-dashboard/css/dicom-viewer.css'
            ],
            tasks: ['dev:banyan']
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    //register tasks
    grunt.registerTask('dev:alemhealth',
        [
            'concat:dashboard_js',
            'concat:dashboard_css'
        ]);


    grunt.registerTask('dev', ['dev:alemhealth']);

};