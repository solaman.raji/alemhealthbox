angular.module('CommonApp.filters').filter('AppStorage', function() {
  return function(input) {

    var use_s3 = $("#use_s3").val();
    var bucket = $("#AWS_STORAGE_BUCKET_NAME").val();
    if( use_s3 == 'False' ){
        var media_url = '/media/'
    }else{
        media_url = 'https://s3-ap-southeast-1.amazonaws.com/' + bucket + '/media/';
    }
    return media_url + input
  };
})