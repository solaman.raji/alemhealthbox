'use strict';

var VALID_DATE_REGEXP = /^[0,1]?\d{1}\/(([0-2]?\d{1})|([3][0,1]{1}))\/(([1]{1}[9]{1}[9]{1}\d{1})|([2-9]{1}\d{3}))$/;


angular.module('CommonApp.directives').directive('validdate', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(value) {

//                if (typeof value === 'string') {
//                  var isValid = dRegex.test(value);
//                  ctrl.$setValidity('date',isValid);
//                  if (!isValid) {
//                    return undefined;
//                  }
//                }

                return value;
              });
                }
    };
});