angular.module('CommonApp.directives').directive('handsometable', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var data = scope.data
            $(element).handsontable({
                data: data,
                colHeaders: ["Name", "Age"],
                rowHeaders: true
            });
        }
    };
});