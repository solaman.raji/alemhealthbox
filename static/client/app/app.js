'use strict';

// Define all your modules with no dependencies

//Common app
angular.module('CommonApp.directives', [])
angular.module('CommonApp', [
    'CommonApp.directives'
]);

angular.module('CommonApp.filters', [])
angular.module('CommonApp', [
    'CommonApp.filters'
]);

//User app
angular.module('UserApp', [
    'ui.router',
    'ngResource',
    'ui.bootstrap',
    'angular-flash.service',
    'angular-flash.flash-alert-directive',
    'googlechart',
    // 'ui.checkbox',
    // 'angularjs-dropdown-multiselect',

    'UserApp.services',
    'UserApp.controllers',
    'UserApp.directives'
]);

// Order app
angular.module('OrderApp', [
    'ui.router',
    'ngResource',
    'ui.bootstrap',
    'angular-flash.service',
    'angular-flash.flash-alert-directive',
    
    //'infinite-scroll',
    'textAngular',
    //'ui.checkbox',
    //'angularjs-dropdown-multiselect',
    //'flow',  // not using
    //'angularFileUpload',

    'OrderApp.services',
    'OrderApp.controllers',
    'OrderApp.directives'
]);



// Lastly, define your "main" module and inject all other modules as dependencies
angular.module('alemhealth', [
    'ui.bootstrap',
    'CommonApp',
    'UserApp',
    'OrderApp',
    // 'ActivityApp',
    'ngResource',
    'ui.router',
    'ngCookies'
]);

// main alemhealth App

angular.module('alemhealth')
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

        var templateDir = '/static/client/app/templates/'

        $stateProvider
            .state('dashboard', {
                url: "",
                abstract: true,
                templateUrl: templateDir + "base.html",
                authenticate: true,
            }).state('dashboard.home', {
                url: "/home",
                views: {
                    'dashboard-view': {
                        controller: 'HomeCtrl',
                        templateUrl: templateDir + "user/home.html"

                    }
                },
                authenticate: true
            }).state('dashboard.broker_home', {
                url: "/broker-home",
                views: {
                    'dashboard-view': {
                        //controller: 'BrokerHomeCtrl',
                        templateUrl: templateDir + "user/broker-home.html"

                    }
                },
                authenticate: true
            }).state('login', {
                url: "/login",
                templateUrl: templateDir + "user/login.html",
                controller: 'LoginCtrl',
                authenticate: false
            }).state('logout', {
                url: "/logout",
                controller: 'LogoutCtrl',
                authenticate: true
            }).state('dashboard.page-not-found', {
                url: "/page-not-found",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + "user/page-not-found.html"
                    }
                }
            });

        $urlRouterProvider.otherwise("/home");


        $httpProvider.interceptors.push('httpRequestInterceptor');
        $httpProvider.interceptors.push('httpResponseInspector');


    }
);

angular.module("alemhealth").factory('httpRequestInterceptor', function ($cookieStore ) {
    return {
        request: function (config) {

            if(config.url.match('^\/static.+html$') != null){
                config.url = config.url + '?' + $("#static_guid").val()
            }
            
            config.headers = {
                'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : '',
                'X-CSRFToken': $('input[name=csrfmiddlewaretoken]').val()
            }


            return config;
        }
    };
});

angular.module("alemhealth").factory('httpResponseInspector', function ($q, $cookieStore) {
    return {
        response: function (response) {
            // do something on success
//            if(response.headers()['content-type'] === "application/json; charset=utf-8"){
//                // Validate response if not ok reject
//                var data = examineJSONResponse(response); // assumes this function is available
//
//                if(!data)
//                    return $q.reject(response);
//            }
            return response;
        },
        responseError: function (response) {
            // do something on error
            if (response.data.code == '403')
                $cookieStore.remove('user');
            return $q.reject(response);
        }
    };
});


angular.module("alemhealth")
    .run(function ($rootScope, $state, $cookieStore, $cookies, $http) {
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            
            // if($cookieStore.get('user') == undefined){
            //     $state.transitionTo("login");
            //     event.preventDefault();

            // }
            // access check here
            if ($cookieStore.get('user') && toState.access) {

                var access = false;
                for (var i = 0; i < toState.access.length; i++) {
                    if ($rootScope.user_group == toState.access[i])
                        access = true

                }

                if (access == false) {
                    $state.transitionTo("dashboard.page-not-found");
                    event.preventDefault();
                }

            }

            if (toState.authenticate && !$cookieStore.get('user')) {
                // User isn’t authenticated
                $state.transitionTo("login");
                event.preventDefault();
            }


        });

        if ($cookieStore.get('user')) {
            $rootScope.email = $cookieStore.get('user').email;
            $rootScope.username = $cookieStore.get('user').username;
            $rootScope.user_group = $cookieStore.get('user').group;
            if($cookieStore.get('user').group=="hospital")
                $rootScope.hospital = $cookieStore.get('user').hospital;
        }

        if($cookieStore.get('hospital')){
            $rootScope.hospital = $cookieStore.get('hospital')
        }


        
        $rootScope.activeTab = ''

    });
