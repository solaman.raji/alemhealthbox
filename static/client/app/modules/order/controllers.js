'use strict'

angular.module('OrderApp.controllers').controller('OrdersCtrl', function($scope, $rootScope, $state, Order, flash, $filter, Hospital){

    
    console.log("Orders Page");
    
    $scope.maxSize = 10;
    $scope.bigCurrentPage = 1;

    $scope.q = undefined;
    $scope.status = undefined;

    $scope.hospital = undefined
    //  get all hospital list

    // Hospital.query({page: -1},
    //     function success(response){
    //         $scope.hospitals = response.hospitals;
    //         //$scope.order.hospital = response.hospitals[0].id
    // });

    $scope.fetchResults = function(){
        flash.info = "Order loading..."
        $scope.results = Order.query({
                page: $scope.bigCurrentPage,
                q: $scope.q,
                status: $scope.status,
                hospital: $scope.hospital
            },
            function success(response){
                flash.info = '';
                $scope.bigTotalItems = response.meta.count;
        });
    }

    $scope.fetchResults();

    $scope.$watch('status', function(){
        if($scope.status != undefined)
            $scope.fetchResults();
    });
    $scope.$watch('hospital', function(){
        if($scope.hospital != undefined)
            $scope.fetchResults();
    });

    $scope.createOrder = function(){
        Order.save({}, function success(data){
            $state.transitionTo('dashboard.order', { guid: data.guid});
        });
    };

    

});

// return a valid date obj from string 

function getDateObj(dateStr, chk_today){
    //debugger
    if(dateStr != ''){

        today = new Date()
        
        date = new Date(
            dateStr.substring(0,4)+'-'+dateStr.substring(4,6)+'-'+dateStr.substring(6,8)
        ) 
        return date
        // if its today then return null string .
        if( chk_today && today.getFullYear() == date.getFullYear() && today.getMonth() == date.getMonth() && today.getDate() == date.getDate() )
             return ''
        else
            return date
        
    }
    else
        return ''
}

// return 2 digit number 
function n(n){
    return n > 9 ? "" + n: "0" + n;
}
// return a valid Dicom date string from Date obj

function getDateStr(dateObj){

    if (dateObj != ''){
        return dateObj.getFullYear().toString()+n(dateObj.getMonth()+1)+n(dateObj.getDate())
    }
}


function getTimeObj(timeStr){
    var d = new Date();
    if(timeStr != ''){
        d.setHours( parseInt(timeStr.substring(0,2)) );
        d.setMinutes( parseInt(timeStr.substring(2,4)) );
    }
    
    return d
}

function getTimeStr(dateObj){
    if (dateObj != ''){
        return n(dateObj.getHours())+n(dateObj.getMinutes())+'00'
    }
}

angular.module('OrderApp.controllers').controller('OrderReportCtrl', function($scope, $stateParams, $cookieStore, $location, $state, $filter, flash, Order){
    // get order details

    flash.info = "Order loading..."
    $scope.order = {}
    Order.get({guid:$stateParams.guid}, function success(data){
        $scope.order = data
        if ( data.status == 0 )
            $scope.method = "Add"
        else{
            $scope.method = "Edit"
            
        }

        flash.info = ""
    });

    // get doctor list for hospital
    $scope.$watch('order.dicoms', function(){

        if($scope.order.dicoms){
            console.log("wathing order dicoms");
            if($scope.order.dicoms){
                loadStudyDicomView($('#testing'), $scope.order, $filter);
            }    
        }
        
    });


});

angular.module('OrderApp.controllers').controller('OrderCtrl', function($scope, $rootScope, $modal, $stateParams, $cookieStore, $location, $state, $filter, $http, Order, Hospital, Provider, Doctor, OrderEmail, DoctorSignature, flash){
    
    
    console.log("Order Page")
    $rootScope.activeTab = 'order'

    if($rootScope.user_group != 'doctor')
        $scope.checkAccess = false
    else
        $scope.checkAccess = true


    $scope.token = $('input[name=csrfmiddlewaretoken]').val();
    $scope.order = {}
    $scope.order.showCreateReport = false;
    $scope.mainContainer = true
    $scope.showReport = false

    // $scope.dateFormat = /^\d{4}(0?[1-9]|1[012])(0?[1-9]|[12][0-9]|3[01])$/
    // $scope.timeFormat = /^(0?[1-9]|1[012]|2[01234])(0?[1-9]|[1-5][0-9]|60)(0?[1-9]|[1-5][0-9]|60)(.*)?$/
    // $scope.ageFormat = /\d{3}[Y,W,D,M]$/
    
    //if($stateParams.report) $scope.showReport = True

    $scope.order.studydate_opened = false
    $scope.radioBtn = {} 
    $scope.radioBtn.serviceType = 'provider'
    
    $scope.openStudyDatepicker = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.order.studydate_opened = true;
    };

    $scope.order.dob_opened = false
    
    
    $scope.openDOBDatepicker = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.order.dob_opened = true;
    };

    $scope.format = 'dd-MMM-yyyy';


    // dicom study date
    $scope.$watch('order.metas.StudyDate', function (data){
        if ( data != undefined ){
            $scope.order.StudyDate = getDateObj(data, false)
        }
    })

    // patient date of birth
    $scope.$watch('order.patient.date_of_birth', function (data){
        if ( data != undefined ){
            $scope.order.date_of_birth = getDateObj(data, true)
        }
    })

    // dicom study time 
    $scope.$watch('order.metas.StudyTime', function (data){
        if ( data != undefined ){
            $scope.order.StudyTime = getTimeObj(data)
        }
    })



    // get order details

    flash.info = "Order loading..."

    Order.get({guid:$stateParams.guid}, function success(data){
        $scope.order = data
        if(data.radiologist) $scope.radioBtn.serviceType = 'radiologist'

        if ( data.status == 0 )
            $scope.method = "Add"
        else{
            $scope.method = "Edit"
        }

        $scope.order.hospital_id = $rootScope.hospital.id
        $scope.order.hospital_name = $rootScope.hospital.name

        flash.info = ""
    });
    
   
    $scope.submitOrder = function(){
        $scope.order.metas.StudyDate = getDateStr($scope.order.StudyDate)
        $scope.order.patient.date_of_birth = getDateStr($scope.order.date_of_birth)
        $scope.order.metas.StudyTime = getTimeStr($scope.order.StudyTime)
        
        // if($scope.radioBtn.serviceType == 'provider'){
        //     $scope.order.radiologist = null
        // }else{
        //     $scope.order.provider = null
        // }

        var modalInstance = $modal.open({
          templateUrl: 'confirmModal.html',
          controller: orderConfirmModalCtrl,
          size: 'lg',
          resolve: {
            data: function () {
               return {
                   'order' : $scope.order
               }
             }
          }
        });

    }

    // GET DR'S 
    Doctor.query({'page':-1},
                function success(response){
                $scope.doctors = response.doctors;
    });

    //  get all provider list

    Provider.query({page: -1},
        function success(response){
            $scope.providers = response.providers;
            //$scope.order.provider = response.providers[0].id
    });



    
    // Email to hospital

    $scope.emailToHospital = function(){
        if ($rootScope.user_group != 'hospital'){
            flash.info = "Sending Email to Hospital"
            OrderEmail.save({guid: $stateParams.guid}, function success(response){
                flash.info = ''
                flash.success = "Email is successfully send to Hospital"
            }, function error(err){
                flash.info = ''
                flash.error = err.data.error
            });
        }else{
            flash.info = "Not allowed to send email"
        }

    };


    // delete modal

    $scope.openDeleteModal = function(){
        var modalInstance = $modal.open({
          templateUrl: 'deleteModal.html',
          controller: orderDeleteModalCtrl,
          size: 'lg'
        });
    }



    // open sms modal
    $scope.openSMSModal = function(text, phone){
        var modalInstance = $modal.open({
          templateUrl: 'smsModal.html',
          controller: smsModelCtrl,
          size: 'sm',
          resolve: {
            data: function () {
               return {
                   'text' : text,
                   'phone' : phone
               }
             }
          }
        });
    }

    // order delivered confirmation modal
    $scope.openOrderDeliveredModal = function(){
        var modalInstance = $modal.open({
          templateUrl: 'orderDelivered.html',
          controller: orderDeliveredModalCtrl,
          size: 'lg'
        });
    }

    // email this report
    $scope.openEmailReportModal = function(){
        var modalInstance = $modal.open({
          templateUrl: 'emailModal.html',
          controller: emailReportMoralCtrl,
          size: 'sm'
        });
    }

    // showReportModal
    $scope.showReportModal = function(){
        var modalInstance = $modal.open({
          templateUrl: 'viewReport.html',
          controller: viewReportModalCtrl,
          size: 'lg',
          resolve: {
            data: function () {
               return {
                   'order' : $scope.order
               }
             }
          }
        });
    }


    // download all dicoms 
    $scope.download_all_dicoms = function(){

        $scope.download_count = 0
        angular.forEach($scope.order.dicoms, function(dicom){ 
            $scope.downloadURL($filter('AppStorage')(dicom.file))

        })
    }


    $scope.downloadURL = function(url){
      var hiddenIFrameID = 'hiddenDownloader' + $scope.download_count++;
      var iframe = document.createElement('iframe');
      iframe.id = hiddenIFrameID;
      iframe.style.display = 'none';
      document.body.appendChild(iframe);
      iframe.src = url;
    }

    // change bill status 

    $scope.changeBillStatus = function(){
        flash.info = "Changing billing status"        
        Order.update({
            guid: $stateParams.guid,
            type: 'billing_status',
            billing_status: $scope.order.billing_status
        }, function success(response){
            flash.info = ''
            flash.success = "Billing status changed successfully"

        },function err(error){
            flash.info = ''
            if( error.status == 400 )
                flash.error = error.data['message']
            else
                flash.error = 'Somethings wrong please try again later';
        });
    }

    window.test = $scope

});



var orderConfirmModalCtrl = function($scope, $stateParams, $modalInstance, $state, flash, data, Order){

    $scope.order = data.order

    $scope.close = function(){
        $modalInstance.close();
    };
    $scope.deleteModel = function(){
        flash.info = 'Order updating on process ...';
        $modalInstance.close();


        Order.update({guid: $stateParams.guid}, $scope.order, function success(response){
                    flash.success = "Order update successfully and start syncing";
                    $state.transitionTo('dashboard.orders');
                },
                function err(error){
                    if( error.status == 400 )
                        flash.error = error.data['message']
                    else
                        flash.error = 'Somethings wrong please try again later';
        });

    };


};


var orderDeleteModalCtrl = function($scope, $stateParams, $modalInstance, $state, flash, Order){
    $scope.close = function(){
        $modalInstance.close();
    };
    $scope.deleteModel = function(){
        flash.info = 'Order deleting on process ...';
        $modalInstance.close();
        Order.delete({ guid: $stateParams.guid }
                ,
                function success(response){
                    flash.success = response.message;
                    $state.transitionTo('dashboard.orders');
                },
                function err(error){
                    if( error.status == 400 )
                        flash.error = error.data['message']
                    else
                        flash.error = 'Somethings wrong please try again later';
                }
        );
    };


};

var smsModelCtrl = function($scope, $stateParams, $modalInstance, $state, flash, SMS, data){


    $scope.data = data

    $scope.close = function(){
        $modalInstance.close();
    };

    $scope.sendSMS = function(){
        flash.info = 'SMS sending'
        SMS.save({
            phone: $scope.data.phone,
            message : $scope.data.message
        }, function success(response){
            flash.info = ''
            flash.success = 'SMS successfully send';
            $modalInstance.close();
        }, function error(err){
            flash.info = ''
            flash.error = err.data.error

        });

    }

};


var orderDeliveredModalCtrl = function($scope, $stateParams, $modalInstance, $state, flash, Order){
    $scope.close = function(){
        $modalInstance.close();
    };
    $scope.orderDelivered = function(){
        $modalInstance.close();

        Order.update({
            guid: $stateParams.guid,
            type: 'report_delivered',
            status: 6
        }, function success(response){
            flash.success = "Report has been marked as delivered"
            $state.transitionTo('dashboard.orders');

        },function err(error){
            if( error.status == 400 )
                flash.error = error.data['message']
            else
                flash.error = 'Somethings wrong please try again later';
        });
    };


};


var  emailReportMoralCtrl = function($scope, $stateParams, $modalInstance, $state, flash, OrderEmail){

    $scope.email = ''
    $scope.close = function(){
        $modalInstance.close();
    };

    $scope.sendEmail = function(){
        flash.info = "Sending Email ..."
        OrderEmail.save({guid: $stateParams.guid, email: $scope.email}, function success(response){
            flash.info = ''
            flash.success = "Email is successfully send"
            $modalInstance.close();
        }, function error(err){
            flash.info = ''
            flash.error = err.data.error
        });
    };


};

var viewReportModalCtrl = function($scope, $stateParams, $modalInstance, $state, flash, data, $filter){

    $scope.order = data.order

    $scope.close = function(){
        $modalInstance.close();
    };

    $scope.report_title = $scope.order.report_filename.split('/')[2]
    PDFJS.getDocument($filter('AppStorage')($scope.order.report_filename)).then(function getPdfHelloWorld(pdf) {
        //
        // Fetch the first page
        //
        pdf.getPage(1).then(function getPageHelloWorld(page) {
          var scale = 1.5;
          var viewport = page.getViewport(scale);
          //
          // Prepare canvas using PDF page dimensions
          //
          var canvas = document.getElementById('the-canvas');
          var context = canvas.getContext('2d');
          canvas.height = viewport.height;
          canvas.width = viewport.width;
          //
          // Render PDF page into canvas context
          //
          var renderContext = {
            canvasContext: context,
            viewport: viewport
          };
          page.render(renderContext);
        });
      });

    $scope.sendEmail = function(){
        flash.info = "Sending Email ..."
        OrderEmail.save({guid: $stateParams.guid, email: $scope.email}, function success(response){
            flash.info = ''
            flash.success = "Email is successfully send"
            $modalInstance.close();
        }, function error(err){
            flash.info = ''
            flash.error = err.data.error
        });
    };
};


