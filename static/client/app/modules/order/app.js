'use strict'

angular.module('OrderApp.services', []);
angular.module('OrderApp.controllers', []);
angular.module('OrderApp.directives', []);

// Order change with "Study" keyword
angular.module('OrderApp').config(function($stateProvider){
    var templateDir = '/static/client/app/templates/order/';

    $stateProvider.state('dashboard.orders', {
        url: '/studies',
        views: {
            'dashboard-view' : {
                controller : 'OrdersCtrl',
                templateUrl : templateDir + 'orders.html'
            }
        },
        authenticate: true,
        access : [ 'admin' , 'operator', 'hospital', 'doctor']

    }).state('dashboard.order', {
        url: '/study/:guid/?report',
        views: {
            'dashboard-view' : {
                controller : 'OrderCtrl',
                templateUrl : templateDir + 'order.html'
            }
        },
        access : [ 'admin' , 'operator', 'hospital', 'doctor']
   }).state('dashboard.report', {
        url: '/report/:guid',
        views: {
            'dashboard-view' : {
                controller : 'OrderReportCtrl',
                templateUrl : templateDir + 'report.html'
            }
        },
        access : [ 'admin' , 'operator', 'hospital', 'doctor']
   }).state('dashboard.reports', {
        url: '/reports/',
        views: {
            'dashboard-view' : {
                controller : 'OrderReportCtrl',
                templateUrl : templateDir + 'reports.html'
            }
        },
        access : [ 'admin']
   })

});


angular.module('OrderApp.controllers').controller('OrderReportCtrl', function($scope, $rootScope, $state, Order, flash, $filter, Hospital, Activity){

    // Check activity  notifications
    Activity.get({'notification':1}, function success(data){
        $rootScope.activities = data.activities
        $rootScope.activities.notification = data.meta.count
    })

    console.log("Orders Page")
    

    $scope.maxSize = 10;
    $scope.bigCurrentPage = 1;

    $scope.q = undefined;
    $scope.status = undefined;

    $scope.hospital = undefined
    //  get all hospital list

    Hospital.query({page: -1},
        function success(response){
            $scope.hospitals = response.hospitals;
            //$scope.order.hospital = response.hospitals[0].id
    });

    $scope.fetchResults = function(){
        flash.info = "Order loading..."
        $scope.results = Order.query({
                page: $scope.bigCurrentPage,
                q: $scope.q,
                status: $scope.status,
                hospital: $scope.hospital
            },
            function success(response){
                flash.info = '';
                $scope.bigTotalItems = response.meta.count;
        });
    }

    $scope.fetchResults();

    $scope.$watch('status', function(){
        if($scope.status != undefined)
            $scope.fetchResults();
    });
    $scope.$watch('hospital', function(){
        if($scope.hospital != undefined)
            $scope.fetchResults();
    });
    $scope.createOrder = function(){
        Order.save({}, function success(data){
            $state.transitionTo('dashboard.order', { guid: data.guid});
        });
    };

});