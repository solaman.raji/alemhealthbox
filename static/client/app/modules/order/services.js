'use restrict'

angular.module('OrderApp.services').factory('Order', function($resource){
   return $resource('/api/orders/:guid', { guid : '@guid' }, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});


angular.module('OrderApp.services').factory('Dicom', function($resource){
   return $resource('/api/orders/dicom/', {}, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});


angular.module('OrderApp.services').factory('SMS', function($resource){
   return $resource('/api/orders/sms/', {}, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});

angular.module('OrderApp.services').factory('OrderEmail', function($resource){
   return $resource('/api/orders/email/', {}, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});


angular.module('OrderApp.services').factory('DoctorSignature', function($resource){
   return $resource('/api/doctor-signature/:id', { id : '@id' }, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});

