'use strict';
angular.module('UserApp.controllers').controller('baseCtrl', function ($scope, $rootScope, $state) {
    console.log("base ctrl")
});

angular.module('UserApp.controllers').controller('HomeCtrl', function ($scope, $rootScope, $state) {
    console.log("Home page");
    $rootScope.activeTab = 'Home'

    // redirect broker user to there home page
//    if($rootScope.user_group == 'broker' )
//        $state.transitionTo('dashboard.broker_home');


    // pie chart
    $scope.chartObject = {};
    $scope.chartObject.data = {"cols": [
        {id: "t", label: "Topping", type: "string"},
        {id: "s", label: "Slices", type: "number"}
    ], "rows": [
        {c: [
            {v: "CT"},
            {v: 20}
        ]},
        {c: [
            {v: "OT"},
            {v: 20}
        ]},
        {c: [
            {v: "DR"},
            {v: 10},
        ]},
        {c: [
            {v: "DS"},
            {v: 20},
        ]},{
         c: [{v: 'NM'},
             {v: 10 }
         ]
        },{
         c: [{v: 'MR'},
             {v: 10 }
         ]
        },{
         c: [{v: 'US'},
             {v: 10 }
         ]
        }
    ]};


    // $routeParams.chartType == BarChart or PieChart or ColumnChart...
    $scope.chartObject.type = 'PieChart';
    $scope.chartObject.cssStyle = "height:500px; width:100%;",
    $scope.chartObject.options = {
        'title': 'Modalities by percentage'
    }


    // line chart

    $scope.lineChart = {};


    $scope.lineChart.data = {"cols": [
        {id: "t", label: "Topping", type: "string"},
        {id: "s", label: "Slices", type: "number"}
    ], "rows": [
        {c: [
            {v: "Draft"},
            {v: 20},
        ]},
        {c: [
            {v: "Order started"},
            {v: 10}
        ]},
        {c: [
            {v: "Image syncing"},
            {v: 10},
        ]},
        {c: [
            {v: "Report pending"},
            {v: 20},
        ]},
        {c: [
            {v: "Report ready"},
            {v: 30},
        ]},
        {c: [
            {v: "Report Delivered"},
            {v: 10},
        ]}
    ]};


    $scope.lineChart.type = 'ColumnChart'
    $scope.lineChart.options = {
        'title': 'Order Status'
    }


    window.test = $scope
});


// ------------------------------------
// User Controllers
// ------------------------------------

angular.module('UserApp.controllers').controller('LoginCtrl', function ($scope, $rootScope, $state, $cookieStore, User, Config, flash) {
    if ($cookieStore.get('user'))
        $state.transitionTo('dashboard.home')

    $scope.user = {}


    $scope.userlogin = function () {

        User.get({ username: $scope.user.username, password: $scope.user.password },
            function success(data) {

                // fetch hospital data 
                Config.get({'key': 'hospital'}, function sucess(data){
                    $cookieStore.put('hospital', data)
                    $rootScope.hospital = $cookieStore.get('hospital')
                })

                $cookieStore.put('user', data)

//                    // set the main dashboard ui router template url
//                    var  sub_domain = data.business.sub_domain;
//                    var patent_state = $state.get('dashboard');
//                    patent_state['templateUrl'] =  '/static/client/templates/' + sub_domain + '/' + patent_state['templateFile'];

                // top header
                $rootScope.username = $cookieStore.get('user').username;
                $rootScope.email = $cookieStore.get('user').email;
                //$rootScope.business_name = $cookieStore.get('user').business.name;

                flash.success = 'Welcome to Alemhealth.';
                $rootScope.user_group = $cookieStore.get('user').group;

                if($cookieStore.get('user').group=="hospital")
                    $rootScope.hospital = $cookieStore.get('user').hospital;

                $state.transitionTo('dashboard.home');

//                if($rootScope.user_group == 'admin')
//                    $state.transitionTo('dashboard.home');
//                else
//                    $state.transitionTo('dashboard.broker_home');
            
                
            },
            function err(error) {
                flash.error = 'Wrong username and password.';
            }
        );
    }
});


angular.module('UserApp.controllers').controller('LogoutCtrl', function ($scope, $cookieStore, $state, flash) {
    $cookieStore.remove('user');
    $cookieStore.remove('hospital');
    $state.transitionTo('login');
});


angular.module('UserApp.controllers').controller('UserCtrl', function ($scope, $rootScope, $state, $cookieStore, User, flash) {

    $scope.user = $cookieStore.get('user');
    console.log($scope.user)
    console.log("user page")

    $scope.saveUser = function () {

        if ($scope.user.new_password != "" && $scope.user.retype_password != "") {
            if ($scope.user.new_password == $scope.user.retype_password)
                $scope.user.password = $scope.user.new_password;
            else {
                flash.error = 'Password don\'t match';
                return false;
            }
        }

        User.update({ username: $scope.user.username }, $scope.user,
            function success(response) {
                $cookieStore.put('user', response)

                // top header
                $rootScope.username = $cookieStore.get('user').username;


                flash.success = 'Acccounts update successfully';
                $state.transitionTo('dashboard.home')
            })
    }
});

angular.module('UserApp.controllers').controller('WebstoreCtrl', function ($scope, $rootScope, $state, $cookieStore, Business, flash) {
    $scope.business = Business.query({});

    $scope.saveBusiness = function () {
        Business.save($scope.business, function success(response) {
            console.log(response)
            $state.transitionTo('dashboard.home')

        }, function err(error) {

        })
    };


});

