'use strict';


angular.module('UserApp.services', [])
angular.module('UserApp.controllers', [])
angular.module('UserApp.directives', [])

angular.module('UserApp')
    .config(function ($stateProvider) {
        var templateDir = '/static/client/app/templates/user/'
        $stateProvider
            .state('dashboard.user', {
                url: "/user",
                views: {
                    'dashboard-view': {
                        templateUrl:  templateDir + "user.html",
                        controller: 'UserCtrl'
                    }
                },
                access : [ 'admin' , 'operator']
            });
    });