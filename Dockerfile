FROM python:2.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD alemhealthbox/requirements.txt /code/
RUN pip install -r requirements.txt
RUN apt-get -qq update
RUN apt-get install -y dcmtk supervisor
COPY ./runserver.sh /
RUN chmod +x /runserver.sh
ENTRYPOINT ["/runserver.sh"]
ADD . /code/
