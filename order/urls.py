from django.conf.urls import url
from order.api import *

urlpatterns = [

    url(r'^api/upload/?$', 'order.api.upload_dicom'),
    url(r'^api/orders/sms/?$', SendSMS.as_view()),
    url(r'^api/orders/email/?$', SendEmail.as_view()),

    url(r'^api/orders/dicom/?$', DicomFileUpload.as_view()),
    url(r'^api/doctor-signature/?$', DoctorSignaturesView.as_view()),
    url(r'^api/doctor-signature/(?P<id>\d+)?$', DoctorSignatureView.as_view()),

    url(r'^api/orders/?$', OrdersView.as_view()),
    url(r'^api/orders/(?P<guid>\w+)?$', OrderView.as_view()),

    #url(r'^dicom-receive/?$', 'receive_dicom', name='receive_dicom'),

    url(r'^dicom_s3_sync_complete/?$', 'order.views.dicom_s3_sync_complete' ),
    url(r'^mirth/?$', 'order.views.mirth'),

]