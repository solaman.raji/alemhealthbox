from celery import task
from django.conf import settings
from alemhealthbox.utils import *


import dicom
import os.path
import os
import shutil
import urllib
import subprocess

from boto.s3.connection import S3Connection
from boto.s3.key import Key

import logging
logger = logging.getLogger('alemhealth')


# task : to store the CSEND image to BOX app

@task(name='add_dicom_from_listener', acks_late=True)
def add_dicom_from_listener(folder_path):
    from order.models import *
    from django.core.files import File
    import os

    # read recent dicom  directories
    dicom_uid = os.path.basename(os.path.dirname(folder_path))[3:]

    # check this order exist or not
    try:
        order = Order.objects.get(dicom_uid=dicom_uid)
    except Order.DoesNotExist:
        order = Order()
        order.dicom_uid = dicom_uid
        order.save()

    logger.info("Order : " + str(order.pk) +
                " start adding dicom from listener folder")

    # read all dicom image within folder
    for dicom_file in os.listdir(folder_path + "/."):

        # check, file is with valid .dcm extension or not and its not
        if dicom_file.endswith('.dcm'):
            try:
                dicom_file_name = dicom_file
                t = dicom_file_name.split(" ")
                # there is space in filename need to replace with '_'
                if len(t) > 1:
                    dicom_file_name = dicom_file_name.replace(" ", "_")
                dicom_save_filename = 'dicoms/' + str(order.guid) + '/' + dicom_file_name

                # already uploaded
                if  not dicom_save_filename in order.dicoms.all().values_list('file', flat=True):

                    # save the file object if not created yet
                    f = open(folder_path + '/' + dicom_file)
                    dcm_file = DicomFile()
                    dcm_file.order = order
                    dcm_file.file.save(dicom_file, File(f))
                    dcm_file.save()
                    f.close()

                    # update after first dicom meta write
                    if order.meta_status == 0:
                        order.meta_status = 1
                        order.save()
                        logger.info("Order : " + str(order.pk) +
                                    " First Dicom : " + str(dcm_file.guid) +
                                    " save successfully ")
                    else:
                        logger.info(
                            "Order : " + str(order.pk) +
                            " Dicom : " + str(dcm_file.guid) +
                            " save successfully "
                        )

                else:
                    logger.info(
                            "Order : " + str(order.pk) +
                            " Dicom : " + str(dicom_file) +
                            " already exist "
                        )
            except Exception as e:
                logger.error("Order : " + str(order.pk) + " Dicom : "
                             + str(dcm_file.guid) + " save failed "
                             + str(e))




# Task : write dicom meta to each order
# This is a async task to write meta to each order


@task(name='dicom_meta_write', acks_late=True)
def dicom_meta_write(guid):
    from order.emails import *
    from order.models import *
    from django.core.files import File


    try:
        order = Order.objects.get(guid=guid)
        logger.info('Order : ' + str(order.pk) + ' start meta writing')
    except Order.DoesNotExist:
        logger.error('Order : ' + str(order.guid) + ' not found')
        return

    count = 0
    for dicom_file in order.dicoms.all():
        try:

            filename = dicom_file.file.name.split('/')[2]
            file_image = dicom.read_file(dicom_file.file)

            file_image.PatientName = str(order.patient.name)
            file_image.PatientID = str(order.patient.patient_id)
            file_image.PatientBirthDate = str(order.patient.date_of_birth)
            file_image.PatientSex = str(order.patient.gender)
            file_image.PatientTelephoneNumbers = str(order.patient.phone)
            file_image.PatientAge = str(order.patient.age)

            file_image.AccessionNumber = str(order.accession_id)
            file_image.Modality = str(order.modality)

            # if order.hospital:
            #     file_image.OperatorsName = str(order.hospital.name)

            file_image.StationName = str(order.hospital_name)
            file_image.ReferringPhysicianName = str(order.doctor.name) if order.doctor else ''

            for meta in order.metas.all():
                # metas[meta.key] = meta.value
                if meta.key in file_image:
                    # print meta.key
                    data_element = file_image.data_element(meta.key)
                    data_element.value = str(meta.value)

                else:
                    setattr(file_image, meta.key, str(meta.value))

            # file_image.BodyPartExamined = "Foot"

            # write the meta
            
                # save to local media
            file_image.save_as(
                settings.BASE_DIR + '/media/dicoms/'
                + str(order.guid) + '/' + filename)
            logger.info("Order : " + str(order.pk) +
                        " Meta write successful in dicom  "
                        + str(dicom_file.id))

            dicom_file.edit_meta = 1
            dicom_file.save()

        except Exception as e:
            logger.error("Order : " + str(order.pk) +
                         " Meta write failed in dicom  "
                         + str(dicom_file.guid) + ' ' + str(e))
            pass

    # check all meta write successfully or not
    if order.dicoms.filter(edit_meta=0).count() == 0:
        # all meta edit successfully start ah sync job
        send_dicom_to_alemhealth.apply_async(
            args=[order.guid])

        # order.meta_status = 2
        # order.status = 2
        # TODO  : when all meta write successfully we'd sync
        # these to provider server, for now we just set it manually
        # order.status = 2 # images synced in provider server
        # order.status = 3 # images synced complete

        # zip dicom files
        ah_temp_order_dir = '/tmp/alemhealth/dicoms/' + str(order.guid) + '/'
        zipfile_name = 'order-' + str(order.pk)

        logger.info(
            "Order : " + str(order.pk) + " Meta write successfully complete ")


    # change the meta status "Write" if all 
    # images meta write completed
    # successfully
    # if not order.dicoms.filter(meta_edit=0).count:
    #     order.meta_status = 2
    #     order.save()
    return


# def send_dicom_to_alemhealth_test():


#     from order.models import *
#     from order.emails import *
#     from django.conf import settings
#     import requests
#     import json
#     import os
#     dicom_dir = settings.BASE_DIR + "/media/"
#     dicom_file_path = dicom_dir + 'dicoms/5c73a3679db4402ba9475396e91fbe70/CT.1.2.392.200036.9116.2.5.1.16.1623985112.1429945247.298310.dcm'
#     url = settings.AH_SERVER + '/api/dicoms/' + '?secret_key=' + settings.CLIENT_SECRET_KEY + '&guid=a10e93255d0d4eb5a2c7364fa09b8a62'
    


#     from poster.encode import multipart_encode
#     from poster.streaminghttp import register_openers
#     import urllib2

#     # Register the streaming http handlers with urllib2
#     register_openers()

#     # Start the multipart/form-data encoding of the file "DSC0001.jpg"
#     # "image1" is the name of the parameter, which is normally set
#     # via the "name" parameter of the HTML <input> tag.

#     # headers contains the necessary Content-Type and Content-Length
#     # datagen is a generator object that yields the encoded parameters
#     datagen, headers = multipart_encode({"file": open(dicom_file_path, "rb")})

#     try:
#         # Create the Request object
#         request = urllib2.Request(url, datagen, headers)
#         # Actually do the request, and get the response
#         response = urllib2.urlopen(request)
#         print response.getcode()
#     except Exception as e:
#         import pdb; pdb.set_trace()
#     return


# Task : send dicom to specific provider by using dcmtk
# This is a async task to send dicom images to there provider

# @task(
#     name='send_dicom_to_alemhealth',
#     bind=True, default_retry_delay=10,
#     max_retries=10
# )
@task(
    name='send_dicom_to_alemhealth',
    acks_late=True,
    bind=True,
    default_retry_delay=2,
    max_retries=10
)
def send_dicom_to_alemhealth(self, guid):

    #guid = '5c73a3679db4402ba9475396e91fbe70'
    from order.models import *
    from order.emails import *
    from django.conf import settings
    import requests
    import json
    from poster.encode import multipart_encode
    from poster.streaminghttp import register_openers
    import urllib2

    # Register the streaming http handlers with urllib2
    register_openers()

    dicom_dir = settings.BASE_DIR + "/media/"
    order_url = settings.AH_SERVER + '/api/orders/' + '?secret_key=' + settings.CLIENT_SECRET_KEY
    try:

        order = Order.objects.get(guid=guid)
        logger.info('Order : ' + str(order.pk) +
                    ' start syncing to Alemhealth ')

        if not order.ah_order_guid:

            data = {
                'hospital_id': order.hospital_id,
                'priority': order.priority,
                'doctor_id': order.doctor_id,
                'provider_id': order.provider_id
            }
            result = requests.post(
                order_url,
                data=json.dumps(data),
                headers={'content-type': 'text/plain'}
            )
            if result.status_code == 200:
                logger.info("order - : " + str(result.json()['id'])
                            + ' successfully added to BOX')
                order.ah_order_id = result.json()['id']
                order.ah_order_guid = str(result.json()['guid'])
                order.save()

            else:
                print 'Some thing wrong in Alemhealth Platform'
                return

        # upload dicoms to AH platform
        for dicom_file in order.dicoms.filter(edit_meta=1):
            dicom_file_path = dicom_dir + dicom_file.file.name

            url = settings.AH_SERVER + '/api/dicoms/' + '?secret_key=' + settings.CLIENT_SECRET_KEY + '&guid=' + str(order.ah_order_guid)

            # headers contains the necessary Content-Type and Content-Length
            # datagen is a generator object that yields the encoded parameters
            datagen, headers = multipart_encode(
                {"file": open(dicom_file_path, "rb")})

            try:
                # Create the Request object
                request = urllib2.Request(url, datagen, headers)
                # Actually do the request, and get the response
                response = urllib2.urlopen(request)

                if response.getcode() == 200:
                    logger.info("order - " + str(order.id)
                                + ' dicom '
                                + str(dicom_file.id) + ' sync to AH successfully ')
                    dicom_file.edit_meta = 2
                    dicom_file.save()

            except Exception as e:
                if self.request.retries >= self.max_retries:
                        # WEHN MAX RETRY EXCEED IT'LL
                        # AGAIN AUTO RETRYING AFTER 60 SEC ..
                    logger.error(
                        "Retrying connect to AH max exit")
                    self.apply_async(args=[guid], countdown=60)
                    return
                else:
                    logger.error(
                        "Retrying connect to AH ")
                    self.retry(exc=str(e))

        # all dycom route successfully
        if order.dicoms.filter(edit_meta=1).count() == 0:

            # send confirmation to AH all dicom synced
            data = {
                'ah_order_guid': order.ah_order_guid,
                'active_status': 1,
                'hospital_id': order.hospital_id
            }
            result = requests.post(
                order_url,
                data=json.dumps(data),
                headers={'content-type': 'text/plain'}
            )
            if result.status_code == 200:

                order.status = 2  # images synced in provider server
                order.save()
                logger.info('Order : ' + str(order.pk) +
                            ' all dicoms successfully sync')
            else:
                print 'Some thing wrong in Alemhealth Platform'
                print 'retrying'
                self.retry()
                #return

    except Order.DoesNotExist:
        logger.error('Order : not found')
        return
    except requests.exceptions.ConnectionError as e:

        if self.request.retries >= self.max_retries:
                # WEHN MAX RETRY EXCEED IT'LL
                # AGAIN AUTO RETRYING AFTER 60 SEC ..
            logger.error(
                "Retrying connect to AH max exit")
            self.apply_async(args=[guid], countdown=60)
            return
        else:
            logger.error(
                "Retrying connect to AH ")
            self.retry(exc=str(e))

    except Exception as e:
        print str(e)

    return


# storescp 5555 -aet alemhealth -fe ".dcm"  -su ah --fork

# Periodic Task :
# store dicom to alemhealth system which get by storescp listener
@task(name='store_dicom_from_listener_dir')
def store_dicom_from_listener_dir():
    from order.models import *
    from django.core.files import File

    storescp_folder_dir = settings.BASE_DIR + "/storescp/"
    for folder_name in os.listdir(storescp_folder_dir + "."):

        # read only directories
        if os.path.isdir(storescp_folder_dir + folder_name):
            dicom_uid = folder_name[3:]

            # check this order exist or not
            try:
                order = Order.objects.get(dicom_uid=dicom_uid)
            except Order.DoesNotExist:

                # save order as draft
                # Todo :
                # need to assign order with a specific operator
                order = Order()
                order.operator_id = 2
                order.dicom_uid = dicom_uid
                order.save()

            # read all dicom image within folder
            for dicom_file in os.listdir(storescp_folder_dir + folder_name + "/."):

                dicom_file_path = 'dicoms/' + \
                    str(order.guid) + '/' + dicom_file

                # check, file is with valid .dcm extension or not and its not
                # already uploaded
                if dicom_file.endswith('.dcm') and not dicom_file_path in order.dicoms.all().values_list('file', flat=True):
                    try:
                        # save the file object if not created yet
                        f = open(storescp_folder_dir + folder_name + '/' + dicom_file)
                        dcm_file = DicomFile()
                        dcm_file.order = order
                        dcm_file.file.save(dicom_file, File(f))
                        dcm_file.save()
                        f.close()
                    except Exception as e:
                        logger.error("Order : " + str(order.pk) + " Dicom : "
                                     + str(dcm_file.guid) + " save failed "
                                     + str(e))

                    # update after first dicom meta write
                    if order.meta_status == 0:
                        order.meta_status = 1
                        order.save()
                        logger.info("Order : " + str(order.pk) +
                                    " First Dicom : " + str(dcm_file.guid) +
                                    " save successfully ")
                    else:
                        logger.info(
                            "Order : " + str(order.pk) +
                            " Dicom : " + str(dcm_file.guid) +
                            " save successfully "
                        )

    return True


# Task : dicom sync complete with desktop app/ mobile app
# we need to collect that dicom and add to our order system as draft

@task(
    name='store_dicom_from_s3_bucket',
    bind=True,
    default_retry_delay=30,
    max_retries=10
)
def store_dicom_from_s3_bucket(self, key, hospital_guid):
    #import pdb; pdb.set_trace()
    from order.models import Order, DicomFile
    from django.core.files import File
    from alemhealth.models import Hospital

    logger.info("Dicom : " + key + " start processing from s3 bucket ")
    dicom_uid = key.split('/')[0]
    dicom_filename = key.split('/')[1]

    
    # Connect to S3
    try:
        conn = S3Connection(
            settings.AWS_ACCESS_KEY_ID,
            settings.AWS_SECRET_ACCESS_KEY,
            host='s3-ap-southeast-1.amazonaws.com'
        )

    except Exception as e:
        logger.error(e)
        if self.request.retries >= self.max_retries:
            logger.error('Cannot connect to s3 : ')
            return False
        # retry gain task
        #store_dicom_from_s3_bucket.retry(exc='Cannot connect to s3 : ')
        return False

    # get bucket
    bucket = conn.get_bucket('alemhealth-desktop')

    try:
        order = Order.objects.get(dicom_uid=dicom_uid)
    except Order.DoesNotExist:

        # save order as draft
        # Todo :
        # need to assign order with a specific operator
        order = Order()
        order.operator_id = 2
        order.dicom_uid = dicom_uid

        if hospital_guid:
            try:
                hospital = Hospital.objects.get(guid=hospital_guid)
                order.hospital = hospital
            except Hospital.DoesNotExist:
                pass

        order.save()

    # create a temp directory
    #temp_directory = settings.BASE_DIR + '/media/temp/'
    temp_directory = '/var/tmp/'
    if not os.path.exists(temp_directory):
        os.makedirs(temp_directory)

    # get the file from s3 bucket
    # store in a temp folder and remove that after use
    try:
        # get the bucket key
        print key
        k = Key(bucket)
        k.key = key
        # make a temp file to store the s3 dicom on that
        temp_dicom_file_path = temp_directory + dicom_filename
        temp_file = open(temp_dicom_file_path, 'w')
        k.get_contents_to_filename(temp_dicom_file_path)
        temp_file.close()

    except Exception as e:
        # todo
        # need to retry again
        logger.error(e)

        if self.request.retries >= self.max_retries:
            logger.error(
                'Order : ' + str(order.pk) +
                ' maximum try to collect dicom ' +
                ' from s3 ' + str(e)
            )
            print 'its return with max try'
            return False
        # retry gain task
        #store_dicom_from_s3_bucket.retry(exc="Issue with bucket " + str(e))
        return False

    # add the temp dicom file to AH system
    try:

        if order.dicoms.all().count() > 0:
            order.meta_status = 1
            order.save()

        f = open(temp_dicom_file_path)
        dcm_file = DicomFile()
        dcm_file.order = order
        dcm_file.file.save(dicom_filename, File(f))
        dcm_file.save()

        logger.info("Dicom : " + key + " successfully add to AH system")
        f.close()

        #remove dicom temp file
        os.remove(temp_dicom_file_path)

    except Exception as e:
        # todo retry
        logger.error("Order : " + str(order.pk) + " Dicom : "
                     + str(dcm_file.guid) + " save failed ")
        if self.request.retries >= self.max_retries:
            logger.error(
                'Order : ' + str(order.pk) +
                ' maximum try to collect ' +
                ' dicom : ' + str(dcm_file.guid) + ' from s3 '
            )
            return False
        # retry gain task
        #store_dicom_from_s3_bucket.retry(exc="problem ")
        return False

    return True

# this task retry after 1 hour with max try 10 times and again start the task


@task(
    name='provider_report_status',
    bind=True, default_retry_delay=60*60,
    max_retries=10
)
def provider_report_status(self, guid):
    from order.models import *
    from order.emails import *

    try:
        order = Order.objects.get(guid=guid)
        logger.info("Order : " + str(order.pk) + " provider status check")
        if order.status == 4:
            # remind_provider_email
            provider_remainder_email(order)
            print "Send remainder email to provider"
            # retry this task
            if self.request.retries >= self.max_retries:
                logger.info(
                    "Maximum try exit, Order : %s retrying provider_report_status in 1 hour " % str(order.pk))
                self.apply_async(args=[guid], countdown=60*60)
                return
            else:
                logger.info('Retrying provider_report_status task in 1 hour')
                self.retry(
                    exc="Order : %s Provider not response with report" % str(order.pk))
        else:
            logger.info("Order : %s Provider respond with report" % str(order.pk))
    except Order.DoesNotExist:
        pass


# predioc 
@task(
    name='check_exchange_rate_in_1_day',
)
def check_exchange_rate_in_1_day():
    logger.info('Check exchange rate')

    from alemhealth.models import Currency
    import requests
    currencies = Currency.objects.all().values_list('code', flat=True)
    currency_str = ''
    count = 0
    for currency in currencies:
        if count:
            currency_str += ',"USD' + currency + '"'
        else:
            currency_str += '"USD' + currency + '"'
        count = count + 1

    if currency_str:
        url = 'http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in (%s)&env=store://datatables.org/alltableswithkeys&format=json' % currency_str

        r = requests.get(url)
        
        if r.status_code == 200:
            
            results = r.json().get('query').get('results').get('rate')

            if count == 1:
                try:
                    currency_obj = Currency.objects.get(code=results.get('id')[3:])
                    old_rate = currency_obj.rate
                    currency_obj.rate = results.get('Rate')
                    currency_obj.save()
                    logger.info('Exchange rate change for ' + results.get('id')[3:] + ' ' + str(old_rate) + ' -> ' + results.get('Rate'))
                except Currency.DoesNotExist:
                    pass
            else :
                for currency in results:
                    try:
                        currency_obj = Currency.objects.get(code=currency.get('id')[3:])
                        old_rate = currency_obj.rate
                        currency_obj.rate = currency.get('Rate')
                        currency_obj.save()
                        logger.info('Exchange rate change for ' + currency.get('id')[3:] + ' ' + str(old_rate) + ' -> ' + currency.get('Rate'))
                    except Currency.DoesNotExist:
                        pass





@task(
    name='another_test_task',
    default_retry_delay=5,
    max_retries=5
)
def another_test_task(t):

    print "another task execute  after " + str(t) + ' Sec'

@task(
    name='test_task',
    bind=True, default_retry_delay=5,
    max_retries=5
)
def test_task(self):
    try:
        print "This is the first task"
        another_test_task.apply_async(args=[60], countdown=60)
    except Exception as e:
        
        if self.request.retries >= self.max_retries:
            print "max Try"    
            self.apply_async(args=[], countdown=5)
            return
        else:
            print 'Retriing 5 sec'
            self.retry(exc=str(e))
            #test_task.apply_async(countdown=10)
    print "DDDD"
