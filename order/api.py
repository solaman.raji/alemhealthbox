from django.views.generic import View
from django.utils.decorators import method_decorator
from django.template.loader import get_template
from django.template import Context
from django.db.models import Q
from django.template.defaultfilters import slugify
from django.core.files import File

import json
import os.path
import datetime
#import xhtml2pdf.pisa as pisa
#import twilio
import mandrill
import base64
import os
import logging

#from twilio.rest import TwilioRestClient

from alemhealthbox.utils import *
from alemhealthbox.decorators import *
from order.models import *
from order.tasks import *
#from activity.models import *

logger = logging.getLogger('alemhealth')


def upload_dicom(request):

    guid = request.POST.get('guid')

    try:
        order = Order.objects.get(guid=guid)
    except Order.DoesNotExist:
        return api_error_message("DoesNotExist", "Order")
    try:
        dcm_file = DicomFile()
        dcm_file.order = order
        dcm_file.file = request.FILES.get('file')
        dcm_file.save()

        data = {"code": "ok"}
        if order.meta_status == 0:
            order.meta_status = 1
            order.save()
            data = {
                'status': 'first'
            }
        logger.info(
                    request.user.email +
                    ' uploaded a new dicom file ' + dcm_file.file.filename +
                    ' for order ' + str(order.pk)
        )
        return dict_to_json(data, 200)
    except Exception as e:
        logger.error(str(e))


class OrderResource(View):

    def format(self, order):

        data = {
            'id': order.pk,
            'guid': str(order.guid),
            'modality': order.modality,
            'accession_id': order.accession_id,
            'name': order.patient.name if order.patient else '',
            'phone': order.patient.phone if order.patient else '',
            'age': order.patient.age if order.patient else '',
            'patient_id': order.patient.patient_id if order.patient else '',
            'created_at': order.created_at.strftime("%d %b, %Y :  %H.%M"),
            #'hospital': order.hospital.name if order.hospital else '',
            #'doctor': order.doctor.name if order.doctor else '',
            'status': order.status,
            'priority': order.priority,
            'status_text': order.get_status_display(),
            'billing_status' : order.billing_status,
            'billing_status_text': order.get_billing_status_display(),
            #'zipfile': str(order.zipfile) if order.zipfile else None,
            #'report_filename': str(order.report_filename) if order.report_filename else None,
            #'provider': order.provider.name if order.provider else None,
            #'price': str(order.receipt.price.unit_price) if order.receipt else None,
            #'radiologist': order.radiologist.name if order.radiologist else None,

        }
        return data

    def detail_format(self, order):
        data = {
            'id': order.pk,
            'guid': str(order.guid),
            'modality': order.modality,
            'accession_id': order.accession_id,
            'metas': {a.key: a.value for a in order.metas.all()},
            'dicoms': [self.dicom_format(a) for a in order.dicoms.all()],
            'status': order.status,
            # 'hospital': order.hospital_id if order.hospital else None,
            # 'hospital_type': order.hospital.hospital_type if order.hospital else None,
            'doctor_name': order.doctor.name if order.doctor else None,
            'doctor': order.doctor_id if order.doctor else None,
            # 'doctor_phone': order.doctor.phone if order.doctor else None,
            'status': order.status,
            'meta_status': order.meta_status,
            'status_text': order.get_status_display(),
            # 'report': order.report,
            # 'doctor_signature': str(order.doctor_signature) if order.doctor_signature else None,
            # 'report_filename': str(order.report_filename) if order.report_filename else None,
            'provider': order.provider_id if order.provider else None,
            'priority': order.priority,
            #'zipfile': str(order.zipfile) if order.zipfile else None,
            # 'billing_status' : order.billing_status,
            # 'doctor_signatures': [self.dr_signature_format(a) for a in order.doctor_signatures.all()],
            # 'activities': [self.activity_format(a) for a in order.activities.all().order_by('-created_at')],
            # 'receipt': str(order.receipt.receipt_pdf) if order.receipt else None,
            # 'radiologist': order.radiologist_id if order.radiologist else None,
            # 'radiologist_name': order.radiologist.name if order.radiologist else None,
            # 'radiologist_signature': str(order.radiologist.signature.file) if order.radiologist and order.radiologist.signature else None,
        }

        if order.patient:
            data['patient'] = {
                'name': order.patient.name,
                'patient_id': order.patient.patient_id,
                'date_of_birth': order.patient.date_of_birth,
                'gender': order.patient.gender,
                'phone': order.patient.phone,
                'age': order.patient.age
            }

        return data

    def meta_format(self, meta):
        return {
            meta.key: meta.value
            # 'value': meta.value
        }

    def dicom_format(self, dicom):
        return {
            'file': str(dicom.file),
            'name': os.path.basename(dicom.file.name),
            # 'size' : dicom.file.size if dicom.file else None
        }

    def dr_signature_format(self, signature):
        return {
            'id': signature.pk,
            'signature': str(signature.signature),
        }

    def activity_format(self, activity):
        data = {
            'id': str(activity.pk),
            'guid': str(activity.guid),
            'actor': activity.actor,
            'context': activity.context,
            'target': activity.target,
            'obj': activity.obj,
            'created_at': str(activity.created_at),
            'mark_as_read': 1
        }
        return data

    def receipt_format(self, data):
        if data:
            return {
                'total_price': str(data.total_price),
                'tax_price': str(data.tax_price),
                'total_price_with_tax': str(data.total_price_with_tax)
            }

class OrdersView(OrderResource):

    @method_decorator(check_accesskey)
    def post(self, request):
        group_name_list = [a.name for a in request.user.groups.all()]

        order = Order()
        order.operator = request.user
        if 'hospital' in group_name_list:
            order.hospital = request.user.hospitals.get(user=request.user)
            if order.hospital.operators.all().count():
                order.operator = order.hospital.operators.all()[0].user

        order.save()
        # logging
        logger.info(
                    request.user.email +
                    ' created a new order ' + str(order.pk)
        )
        # add activity

        actor = {
            'type': 'user',
            'name': request.user.first_name + ' ' + request.user.last_name,
            'guid': str(request.user.profile.guid)
        }

        context = 'initiated'

        obj = {
            'type': 'study',
            'name': 'Draft study #' + str(order.pk),
            'guid': str(order.guid)
        }
        listeners = [request.user]
        if order.hospital:
            listeners.append(order.operator)

        create_activity(
            actor, context, listeners, order=order, obj=obj, user=request.user)

        data = {
                'guid': str(order.guid),
                'status': order.status,
                'id': order.pk
        }
        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def get(self, request):

        post_per_page = 20
        page = request.GET.get('page')
        q = request.GET.get('q')
        status = request.GET.get('status')
        hospital = request.GET.get('hospital')

        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        # order list access wise
        group_name_list = [a.name for a in request.user.groups.all()]

        if 'operator' in group_name_list:
            orders = Order.objects.filter(
                operator=request.user).order_by('-updated_at')
        elif 'hospital' in group_name_list:
            hospital = request.user.hospitals.all()[0]
            orders = Order.objects.filter(
                hospital=hospital).order_by('-updated_at')
        elif 'doctor' in group_name_list:
            if request.user.doctor.radiologist:
                orders = Order.objects.filter(
                    radiologist__user=request.user).order_by('-updated_at')
            else:
                orders = Order.objects.filter(
                    doctor__user=request.user).order_by('-updated_at')
        else:
            orders = Order.objects.all().order_by('-updated_at')

        if hospital:
            orders = orders.filter(hospital_id=hospital)
        if q:
            orders = orders.filter(
                Q(patient__name__icontains=q) |
                Q(patient__gender__icontains=q) |
                Q(patient__age__icontains=q) |
                Q(patient__phone__icontains=q) |
                Q(patient__blood_group__icontains=q) |
                Q(patient__city__icontains=q) |

                Q(doctor__name__icontains=q) |
                Q(doctor__specialty__icontains=q) |
                Q(doctor__email__icontains=q) |
                Q(doctor__phone__icontains=q) |
                Q(doctor__city__icontains=q) |


                Q(hospital__name__icontains=q) |
                Q(hospital__phone__icontains=q) |
                Q(hospital__city__icontains=q) |
                Q(hospital__address__icontains=q) |
                Q(hospital__general_email__icontains=q) |
                Q(hospital__primary_name__icontains=q) |
                Q(hospital__primary_email__icontains=q) |
                Q(hospital__primary_physician_contact__icontains=q) |


                Q(accession_id__icontains=q) |
                Q(modality__icontains=q) |
                Q(speciality__icontains=q) |
                Q(history__icontains=q)
            )
        if status:
            orders = orders.filter(status=status)

        count = orders.count()
        orders = orders[offset:pagesize]
        data = {}

        data['meta'] = {
            "offset": offset,
            "pagesize": pagesize,
            "count": count
        }
        data['orders'] = [self.format(a) for a in orders]

        return dict_to_json(data, 200)


class OrderView(OrderResource):

    @method_decorator(check_accesskey)
    def put(self, request, guid):
        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message('DoesNotExist', "Order")

        json_dict = json.loads(request.body)

        # start dicom routing
        if json_dict.get('type') == 'start_routing' and order.provider:
            order.status = json_dict.get('status')
            order.save()
            data = {}
            data = self.detail_format(order)

            logger.info(
                request.user.email +
                'routed'
                + order.provider.name
            )

            return dict_to_json(data, 200)
        #import pdb; pdb.set_trace()
        if json_dict.get('doctor'):
            try:
                doctor = Doctor.objects.get(pk=json_dict.get('doctor'))
                order.doctor = doctor
            except Doctor.DoesNotExist:
                pass

        if json_dict.get('provider'):
            try:
                provider = Provider.objects.get(pk=json_dict.get('provider'))
                order.provider = provider
            except Provider.DoesNotExist:
                pass
        else:
            order.provider = None

        try:

            order.modality = json_dict.get('modality')
            order.priority = json_dict.get('priority')
            order.hospital_id = json_dict.get('hospital_id')
            order.hospital_name = json_dict.get('hospital_name')
            order.status = 1
            order.meta_status = 1
            order.ah_order_id = 0
            order.save()
        except Exception as e:
            print str(e) # import pdb; pdb.set_trace()

        if json_dict.get('metas'):
            for meta in json_dict.get('metas'):
                try:
                    order_meta = OrderMeta.objects.get(
                        order=order,
                        key=meta
                    )

                    order_meta.value = json_dict.get('metas').get(meta)
                    order_meta.save()
                except OrderMeta.DoesNotExist:
                    pass

        data = self.detail_format(order)

        # save patient

        if json_dict.get('patient'):

            order.patient.name = json_dict.get('patient')['name']
            #order.patient.patient_id = json_dict.get('patient')['patient_id']
            order.patient.gender = json_dict.get('patient')['gender']
            order.patient.date_of_birth = json_dict.get('patient')['date_of_birth']
            order.patient.phone = json_dict.get('patient')['phone']

            patient_dob = datetime.datetime.strptime(order.patient.date_of_birth, '%Y%m%d')
            order.patient.age = str(age(patient_dob)).zfill(3) + 'Y'
            order.patient.save()

        # add celery task for edit meta
        dicom_meta_write.delay(guid=order.guid)

        # logger.error('Order : ' + str(order.pk) + ' Can\'t start task ' + str(e) )
        # dicom_meta_write(order.guid)

        # logger 

        logger.info(
            request.user.email +
            ' edited order '
            + str(order.pk)
        )

        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def get(self, request, guid):

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "order")

        data = self.detail_format(order)
        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def delete(self, request, guid):

        try:
            order = Order.objects.get(guid=guid)
            id = order.pk
            order.delete()
            logger.info(
                request.user.email +
                ' deleted order : '
                + str(id)
            )

        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", 'Order')

        return api_error_message("DeleteSuccessfully", "Order")


def pdf_generate(order):

    data = {}
    data['order'] = order
    data['settings'] = settings
    data['create_time'] = str(datetime.date.today())
    # Render html content through html template with context
    template = get_template('report/order.html')
    html = template.render(Context(data))

    filename = str(order.pk) + '_' + slugify(order.accession_id) + '_' + '_' + slugify(str(datetime.date.today()))
    filename = filename[:120]
    filename = filename + '.pdf'

    tmp_filename = '/tmp/alemhealth/' + filename
    # Write PDF to file
    tmp_file = open(tmp_filename, "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=tmp_file)

    if pisaStatus:
        order.report_filename.save(filename, File(tmp_file))
        # order.save()

        if os.path.isfile(tmp_filename):
            os.unlink(tmp_filename)

    tmp_file.close()

    return pisaStatus
    # # Return PDF document through a Django HTTP response
    # file.seek(0)
    # pdf = file.read()
    # file.close()            # Don't forget to close the file handle
    # return HttpResponse(pdf, mimetype='application/pdf')

def create_receipt(order):
    #import pdb; pdb.set_trace()
    data = {}
    data['order'] = order
    data['settings'] = settings
    data['create_time'] = str(datetime.date.today())
    # Render html content through html template with context
    template = get_template('report/receipt.html')
    html = template.render(Context(data))

    filename = 'receipt-' + slugify(order.accession_id)
    filename = filename[:120]
    filename = filename + '.pdf'

    tmp_filename = '/tmp/alemhealth/' + filename
    # Write PDF to file
    tmp_file = open(tmp_filename, "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=tmp_file)

    if pisaStatus:
        order.receipt.receipt_pdf.save(filename, File(tmp_file))
        # order.save()

        if os.path.isfile(tmp_filename):
            os.unlink(tmp_filename)

    tmp_file.close()

    return pisaStatus



class DicomFileUpload(OrderResource):

    @method_decorator(check_accesskey)
    def post(self, request):
        guid = request.POST.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")
        #import pdb;pdb.set_trace()
        #if request.FILES.get('file').content_type == 'application/dicom':
        dcm_file = DicomFile()
        dcm_file.order = order
        dcm_file.file = request.FILES.get('file')
        dcm_file.save()




        if order.meta_status == 0 and order.dicoms.all().count() > 0:
            order.meta_status = 1
            order.save()
            data = self.detail_format(order)
        else:
            data = {}

        return dict_to_json(data, 200)


class DoctorSinatureResource(View):
    def format(self, signature):
        return {
            'id': signature.pk,
            'signature': str(signature.signature)
        }


class DoctorSignaturesView(DoctorSinatureResource):

    @method_decorator(check_accesskey)
    def post(self, request):
        guid = request.POST.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")

        doctor_signature = DoctorSignature()
        doctor_signature.signature = request.FILES.get('file')
        doctor_signature.order = order
        doctor_signature.save()

        data = {}
        data = self.format(doctor_signature)

        return dict_to_json(data, 200)


class DoctorSignatureView(DoctorSinatureResource):
    @method_decorator(check_accesskey)
    def delete(self, request, id):

        guid = request.GET.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")
        try:
            doctor_signature = DoctorSignature.objects.get(order=order, pk=id)
            doctor_signature.signature.delete()
            doctor_signature.delete()
        except DoctorSignature.DoesNotExist:
            return api_error_message("DoesNotExist", "Doctor Signature")

        data = {}
        return dict_to_json(data, 200)


class DoctorFileUpload(OrderResource):
    @method_decorator(check_accesskey)
    def post(self, request):

        guid = request.POST.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")

        if order.doctor_signature:
            order.doctor_signature.delete()
            # if settings.USE_S3:

            # else:
            #     file_path = str(order.doctor_signature.file)
            #     if os.path.isfile(file_path):
            #             os.unlink(file_path)

        order.doctor_signature = request.FILES.get('file')
        order.save()

        data = {}
        data = self.detail_format(order)

        return dict_to_json(data, 200)


class SendSMS(OrderResource):
    @method_decorator(check_accesskey)
    def post(self, request):

        json_dict = json.loads(request.body)
        client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

        try:
            message = client.messages.create(to=json_dict.get('phone'), from_="+13232444448",body=json_dict.get('message'))

        except twilio.TwilioRestException as e:
            return dict_to_json({'error': e.msg}, 501)

        return dict_to_json({}, 200)


class SendEmail(OrderResource):
    @method_decorator(check_accesskey)
    def post(self, request):
        json_dict = json.loads(request.body)
        guid = json_dict.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")

        if json_dict.get('email'):
            email = json_dict.get('email')
        else:
            email = order.hospital.general_email

        try:
            #import pdb; pdb.set_trace()
            mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)
            #report_file = settings.MEDIA_ROOT + '/' + 'report-' + str(order.pk) + '.pdf'
            report_file = order.report_filename.read()
            filename = 'Report-' + str(order.pk)
            # send email to sign up user
            message = {
                'from_email': 'team@alemhealth.com',
                'from_name': 'Alemhealth team',
                'headers': {'Reply-To': 'message.reply@example.com'},
                'html': "Dear "+ order.hospital.name +",<br/>"
                        "Attached is a report of "+ order.modality+" of "+ order.patient.name +" submitted on " + order.created_at.strftime("%d %b, %Y :  %H.%M") + ".<br/> "
                        "Alemhealth Office.",

                'important': True,
                'subject': 'Report - ' + str(order.pk),
                'tags': ['password-resets'],
                'to': [{'email': email, 'name': order.hospital.name }],
                'attachments' : [{
                    'type' :'application/pdf',
                    'name' :filename,
                    'content': base64.b64encode(report_file)
                }]
            }
            result = mandrill_client.messages.send(message=message, async=False, ip_pool='Main Pool', send_at='')

            if result[0]['status'] != "sent" and result[0]['status'] != "queued" :
                code = 501
                return dict_to_json({'error': 'Somethings goes wrong, Please try again later.'}, 501)

        except mandrill.Error, e:
            return dict_to_json({'error': e.msg}, 501)

        return dict_to_json({}, 200)
