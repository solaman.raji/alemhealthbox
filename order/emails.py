import mandrill
from django.conf import settings
from alemhealthbox.utils import *



from django.template.loader import render_to_string
# from django.shortcuts import render_to_response
import logging
logger = logging.getLogger('alemhealth')



# send a email when all dicom successfully route to provider
def send_provider_dicom_route_email(order):
    # guid = '2414f2a4b6a94109be3eba3072383dd1'
    # from order.models import *
    # order = Order.objects.get(guid=guid)
    if not order.provider:
        return

    email_template = render_to_string(
        'email/order/provider_email.html', {
            'order': order,
            'history': order.metas.get(key="AdditionalPatientHistory").value
        }
    )
    email_list = []
    for email in order.provider.primary_email.split(' '):
        if re.match(r"^[a-zA-Z0-9._]+\@[a-zA-Z0-9._]+\.[a-zA-Z]{2,}$", email)!=None:
            email_list.append({'email': email})
    if not email_list:
        return

    logger.info(
        'Order : ' + str(order.pk) +
        ' Sending dicom route confirmation emails to ' +
        order.provider.primary_email)

    try:
        mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)

        message = {
            'from_email': 'team@alemhealth.com',
            'from_name': 'Alemhealth',
            'headers': {'Reply-To': 'noreply@alemhealth.com'},
            'html': email_template,
            'important': True,
            'subject': 'Accession: ' + order.accession_id,
            'tags': ['provider-notification'],
            'to': email_list,
        }

        result = mandrill_client.messages.send(
            message=message, async=False, ip_pool='Main Pool', send_at='')

        if result[0]['status'] != "sent" and result[0]['status'] != "queued":
            logger.error('Queued')

    except mandrill.Error, e:
        logger.error(str(e))

    return


# send operator report ready email
def send_operator_report_ready(order):
    if not order.operator:
        return

    email_template = render_to_string(
        'email/order/operator_report_ready_email.html', {
            'order': order
        }
    )

    if not order.operator.email:
        return

    logger.info(
        'Order : ' + str(order.pk) +
        ' Sending operator report ready  email to ' +
        order.operator.email)
    try:
        mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)

        message = {
            'from_email': 'team@alemhealth.com',
            'from_name': 'Alemhealth',
            'headers': {'Reply-To': 'noreply@alemhealth.com'},
            'html': email_template,
            'important': True,
            'subject': order.priority + '-' + order.patient.patient_id + '-Study Reported',
            'tags': ['operator-report-ready'],
            'to': [
                {
                    'email': order.operator.email,
                    'name': order.operator.first_name +
                    order.operator.last_name
                }],
        }

        result = mandrill_client.messages.send(
            message=message, async=False, ip_pool='Main Pool', send_at='')

        if result[0]['status'] != "sent" and result[0]['status'] != "queued":
            logger.error('Queued')

    except mandrill.Error, e:
        logger.error(str(e))
    return

# send provider remainder email


def provider_remainder_email(order):
    from activity.models import create_activity
    if not order.provider:
        return
    try:
        email_template = render_to_string(
            'email/order/provider_remainder.html', {
                'order': order
            }
        )
        email_list = []
        for email in order.provider.primary_email.split(' '):
            if re.match(r"^[a-zA-Z0-9._]+\@[a-zA-Z0-9._]+\.[a-zA-Z]{2,}$", email)!=None:
                email_list.append({'email': email})
        if not email_list:
            return

        logger.info(
            'Order : ' + str(order.pk) +
            ' Sending provider remainder emails to ' +
            order.provider.primary_email)


        mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)

        message = {
            'from_email': 'team@alemhealth.com',
            'from_name': 'Alemhealth',
            'headers': {'Reply-To': 'noreply@alemhealth.com'},
            'html': email_template,
            'important': True,
            'subject': order.priority + ' 1 hour reminder ' + order.patient.patient_id,
            'tags': ['provider-remainder'],
            'to': email_list,
        }

        result = mandrill_client.messages.send(
            message=message, async=False, ip_pool='Main Pool', send_at='')

        if result[0]['status'] != "sent" and result[0]['status'] != "queued":
            logger.error('Queued')

        # add activity

        actor = {
            'type': 'study',
            'name': order.accession_id,
            'guid': str(order.guid)
        }

        context = 'Study Not Reported: Reminder sent to'

        obj = {
            'type': 'provider',
            'name': order.provider.name,
            'guid': str(order.provider.guid)
        }

        listeners = [order.operator, order.hospital.user.all()[0]]

        create_activity(actor, context, listeners, obj=obj, order=order)

    except Exception as e:
        logger.error(str(e))

    except mandrill.Error, e:
        logger.error(str(e))

    return

# send a email to Radiologist while its assign to him
def radiologist_new_study_email(order):
    # guid = '2414f2a4b6a94109be3eba3072383dd1'
    # from order.models import *
    # order = Order.objects.get(guid=guid)
    if not order.radiologist or not order.radiologist.user.email:
        return

    email_template = render_to_string(
        'email/order/radiologist_new_study.html', {
            'order': order,
            'history': order.metas.get(key="AdditionalPatientHistory").value
        }
    )
    send_email = order.radiologist.user.email


    logger.info(
        'Order : ' + str(order.pk) +
        ' Sending new study ready for radiologist  ' +
        send_email)

    try:
        mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)

        message = {
            'from_email': 'team@alemhealth.com',
            'from_name': 'Alemhealth',
            'headers': {'Reply-To': 'noreply@alemhealth.com'},
            'html': email_template,
            'important': True,
            'subject': 'Accession: ' + order.accession_id,
            'tags': ['radiologist-new-study'],
            'to': [
                {
                    'email': send_email,
                    'name': order.radiologist.user.first_name +
                    order.radiologist.user.last_name
                }],
        }

        result = mandrill_client.messages.send(
            message=message, async=False, ip_pool='Main Pool', send_at='')

        if result[0]['status'] != "sent" and result[0]['status'] != "queued":
            logger.error('Queued')

    except mandrill.Error, e:
        logger.error(str(e))

    return
