from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from django.core.files import File
import sys
import os
from order.models import *

import logging
logger = logging.getLogger('alemhealth')


#from order.tasks import add_dicom_from_listener

class Command(BaseCommand):
    args = 'dicom_file_path'
    help = 'Add dicom from storescp to AH platform'

    def handle(self, *args, **options):

        folder_path = args[0]


        # read recent dicom  directories
        dicom_uid = os.path.basename(folder_path)[3:]

        # check this order exist or not
        try:
            order = Order.objects.get(dicom_uid=dicom_uid)
        except Order.DoesNotExist:
            order = Order()
            order.dicom_uid = dicom_uid
            order.save()
        
        logger.info("Order : " + str(order.pk) +
                            " start adding dicom from listener folder")

        # read all dicom image within folder
        for dicom_file in os.listdir(folder_path + "/."):

            # dicom_file_path = 'dicoms/' + \
            #     str(order.guid) + '/' + dicom_file

            # check, file is with valid .dcm extension or not and its not
            if dicom_file.endswith('.dcm'):
                try:
                    dicom_file_name = dicom_file
                    t = dicom_file_name.split(" ")
                    # there is space in filename need to replace with '_'
                    if len(t) > 1:
                        dicom_file_name = dicom_file_name.replace(" ", "_")
                    dicom_save_filename = 'dicoms/' + str(order.guid) + '/' + dicom_file_name

                    # already uploaded
                    if  not dicom_save_filename in order.dicoms.all().values_list('file', flat=True):

                        # save the file object if not created yet
                        f = open(folder_path + '/' + dicom_file)
                        dcm_file = DicomFile()
                        dcm_file.order = order
                        dcm_file.file.save(dicom_file, File(f))
                        dcm_file.save()
                        f.close()

                        # update after first dicom meta write
                        if order.meta_status == 0:
                            order.meta_status = 1
                            order.save()
                            logger.info("Order : " + str(order.pk) +
                                        " First Dicom : " + str(dcm_file.guid) +
                                        " save successfully ")
                        else:
                            logger.info(
                                "Order : " + str(order.pk) +
                                " Dicom : " + str(dcm_file.guid) +
                                " save successfully "
                            )

                    else:
                        logger.info(
                                "Order : " + str(order.pk) +
                                " Dicom : " + str(dicom_file) +
                                " already exist "
                            )
                except Exception as e:
                    logger.error("Order : " + str(order.pk) + " Dicom : "
                                 + str(dcm_file.guid) + " save failed "
                                 + str(e))



    #add_dicom_from_listener.delay(file_path)


        # storescp 5555 -aet alemhealth -xcs ". /Users/macbookpro/development/python/virtual-env/box/bin/activate; cd /Users/macbookpro/development/python/alemhealthbox/; python manage.py add_dicom #p"  --log-config /Users/macbookpro/development/python/alemhealthbox/dcmtk_log_config.txt -od /Users/macbookpro/development/python/alemhealthbox/dicoms/ -su ah -tos 30 --accept-all
        # storescp 5555 -aet alemhealth -xcs ". /Users/macbookpro/development/python/virtual-env/box/bin/activate; cd /Users/macbookpro/development/python/alemhealthbox/; python manage.py add_dicom #p"  --log-config /Users/macbookpro/development/python/alemhealthbox/dcmtk_log_config.txt -od /Users/macbookpro/development/python/alemhealthbox/dicoms/ -su ah -tos 3 --accept-all -fe ".dcm"



