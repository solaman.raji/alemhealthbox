from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
# Create your models here.

from uuidfield import UUIDField

from alemhealthbox.models import *
from order.tasks import *
from order.emails import *
import dicom
import os
import shutil
import datetime

class Order(models.Model):

    modality_list = (
        ('CT', 1),
        ('NM', 2),
        ('MR', 3),
        ('DS', 4),
        ('DR', 5),
        ('US', 6),
        ('OT', 7),
        ('HSG', 8),
        ('CR', 9),
    )

    priority_list = (
        ('Routine', 'Routine'),
        ('STAT', 'STAT')
    )

    status_list = (
        (0, 'Draft study'),
        (1, 'Study syncing'),
        (2, 'Study sync completed'),
    )

    meta_status_list = (
        (0, 'Not extracted'),
        (1, 'Extracted'),
        (2, 'Meta write'),
    )

    billing_status_list = (
        (0, 'Not billed'),
        (1, 'Free Study'),
        (2, 'Invoice Pending'),
        (3, 'Paid'),
    )

    guid = UUIDField(auto=True)
    dicom_uid = models.TextField(null=True, blank=True)

    # associations
    # operator = models.ForeignKey(
    #     User,
    #     related_name='orders',
    #     blank=True,
    #     null=True,
    #     on_delete=models.SET_NULL
    # )

    patient = models.ForeignKey(
        Patient,
        related_name='orders',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    doctor = models.ForeignKey(
        Doctor, related_name='orders', blank=True, null=True)
    hospital_id = models.IntegerField(
         blank=True, null=True)
    hospital_name = models.CharField(max_length=128, null=True, blank=True)

    provider = models.ForeignKey(
        Provider,
        related_name='orders',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    # radiologist = models.ForeignKey(
    #     Doctor,
    #     related_name='radiologist_orders',
    #     blank=True,
    #     null=True,
    #     on_delete=models.SET_NULL
    # )

    accession_id = models.CharField(max_length=128, null=True, blank=True)
    modality = models.CharField(
        max_length=10, null=True, blank=True, choices=modality_list)
    speciality = models.TextField(null=True, blank=True)
    history = models.TextField(null=True, blank=True)

    priority = models.CharField(
        max_length=12, choices=priority_list, blank=True, null=True)
    status = models.IntegerField(max_length=1, choices=status_list, default=0)

    meta_status = models.IntegerField(
        max_length=1, choices=meta_status_list, default=0)
    billing_status = models.IntegerField(
        max_length=1, choices=billing_status_list, default=0)


    ah_order_id = models.IntegerField(null=True, blank=True)
    ah_order_guid = models.CharField(max_length=128, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'orders'

    def __unicode__(self):
        return str(self.guid)

    def save(self, *args, **kwargs):
        super(Order, self).save(**kwargs)
        # start dicom routing to Provider
        # if self.status == 2:
        #     send_dicom_to_provider.delay(guid=self.guid)
        #     # send_dicom_to_provider(guid=self.guid)
        # if self.status == 5:
        #     send_operator_report_ready(self)


# delete dicom files when order delete

def delete_dicom_files(sender, instance, **kwargs):

    if settings.USE_S3:
        for dicom_obj in instance.dicoms.all():
            dicom_obj.file.delete()

    else:
        folder = settings.BASE_DIR + '/media/dicoms/' + str(instance.guid)
        if(os.path.isdir(folder)):
            for the_file in os.listdir(folder):
                file_path = os.path.join(folder, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                except Exception, e:
                    print e
            try:
                shutil.rmtree(folder)
            except Exception, e:
                print e


models.signals.pre_delete.connect(delete_dicom_files, sender=Order)


class OrderMeta(models.Model):

    order = models.ForeignKey(Order, related_name='metas')
    key = models.TextField()
    value = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'order_metas'

    def __unicode__(self):
        return self.key


def dicom_file_upload_dir(instance, filename):

        t = filename.split(" ")
        # there is space in filename need to replace with '_'
        if len(t) > 1:
            filename = filename.replace(" ", "_")
        return 'dicoms/' + str(instance.order.guid) + '/' + filename


class DicomFile(models.Model):

    edit_meta_list = (
        (0, 'Not edit'),
        (1, 'Edit'),
        (2, 'Send'),
    )

    transfer_syntax_list = (
        ('LittleEndianImplicit', ''),
        ('LittleEndianExplicit', ''),
        ('DeflatedExplicitVRLittleEndian', ''),
        ('BigEndianExplicit', ''),
        ('JPEGProcess1', ''),
        ('JPEGProcess2_4', ''),
        ('JPEGProcess14SV1', ''),
        ('JPEGLSLossless', '--propose-jls-lossless'),
        ('JPEGLSLossy', '--propose-jls-lossy'),
        ('JPEG2000LosslessOnly', '--propose-j2k-lossless'),
        ('JPEG2000' , '--propose-j2k-lossy'),
        ('MPEG2MainProfileAtMainLevel', '--propose-mpeg2'),
        ('MPEG2MainProfileAtHighLevel', '--propose-mpeg2-high'),
        ('RLELossless', '--propose-rle'),
    )
    guid = UUIDField(auto=True)
    order = models.ForeignKey(Order, related_name='dicoms')
    file = models.FileField(upload_to=dicom_file_upload_dir, max_length=320)

    edit_meta = models.IntegerField(
        max_length=1, choices=edit_meta_list, default=0)

    transfer_syntax = models.CharField(
        max_length=128, blank=True, null=True, choices=transfer_syntax_list)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'dicom_images'

    def __unicode__(self):
        return str(self.guid)

    def save(self, *args, **kwargs):

        # check dicom filename  has space or not

        super(DicomFile, self).save(**kwargs)

        # if self.file.name:
        #     t = self.file.name.split(" ")
        #     # there is space in filename need to replace with '_'
        #     if len(t) > 1:
        #         filename = settings.BASE_DIR + '/media/' + self.file.name
        #         os.rename(filename, filename.replace(" ", "_"))
        #         self.file.name = self.file.name.replace(" ", "_")
        #         super(DicomFile, self).save(**kwargs)

        # extract meta data for first time
        if self.order.meta_status == 0:

            try:
                dicom_file = dicom.read_file(self.file)
            except Exception:
                return
            # add new patient
            # todo: get or create patient 

            patient, created = Patient.objects.get_or_create(
                name=str(dicom_file.get('PatientName')),
                gender=str(dicom_file.get('PatientSex')),
                phone=str(dicom_file.get('PatientTelephoneNumbers'))
            )
            patient.date_of_birth = str(dicom_file.get('PatientBirthDate'))
            if patient.date_of_birth:
                patient_dob = datetime.datetime.strptime(patient.date_of_birth, '%Y%m%d')
                patient.age = str(age(patient_dob)).zfill(3) + 'Y'
            

            patient.patient_id = 'P-' + str(patient.pk)
            patient.save()

            self.order.patient = patient

            self.order.modality = str(dicom_file.get('Modality'))
            # self.order.accession_id = str(dicom_file.get('AccessionNumber'))
            self.order.save()

            # add all metas

            OrderMeta.objects.bulk_create([
                OrderMeta(
                    order=self.order, key='BodyPartExamined',
                    value=str(dicom_file.get('BodyPartExamined'))),
                # OrderMeta(
                #     order=self.order, key='DateofSecondaryCapture',
                #     value=str(dicom_file.get('DateofSecondaryCapture'))),
                # OrderMeta(
                #     order=self.order, key='TimeofSecondaryCapture',
                #     value=str(dicom_file.get('TimeofSecondaryCapture'))),
                OrderMeta(
                    order=self.order, key='StudyDate',
                    value=str(dicom_file.get('StudyDate'))),
                OrderMeta(
                    order=self.order, key='StudyTime',
                    value=str(dicom_file.get('StudyTime'))),
                OrderMeta(
                    order=self.order, key='InstitutionName',
                    value='Alem Health'),
                OrderMeta(
                    order=self.order, key='InstitutionAddress',
                    value="Block B, Core F Unit 304 Downtown, Dubai, UAE"),
                # OrderMeta(order=self.order, key='ReferringPhysician',
                #    value=str(dicom_file.get('ReferringPhysician'))),
                OrderMeta(
                    order=self.order, key='StudyDescription',
                    value=str(dicom_file.get('StudyDescription'))),
                OrderMeta(
                    order=self.order, key='SeriesDescription',
                    value=str(dicom_file.get('SeriesDescription'))),
                OrderMeta(
                    order=self.order, key='OperatorsName',
                    value=str(dicom_file.get('OperatorsName'))),
                OrderMeta(
                    order=self.order, key='TransferSyntax',
                    value=str(dicom_file.get('TransferSyntax'))),

                OrderMeta(
                    order=self.order, key='AdditionalPatientHistory',
                    value=str(dicom_file.get('AdditionalPatientHistory'))),

            ])




