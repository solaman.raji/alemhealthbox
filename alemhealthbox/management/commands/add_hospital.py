from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import requests

from alemhealthbox.models import *

class Command(BaseCommand):
    args = 'hospital_guid'
    help = 'Add hospital to Alemhealth Box'

    def handle(self, *args, **options):

        try:
            guid = args[0]
            
            url = settings.AH_SERVER + '/api/hospitals/' + guid + '?secret_key=' + settings.CLIENT_SECRET_KEY
            print url
            result = requests.get(url)

            if result.status_code == 200:
                config, created = Config.objects.get_or_create(key='hospital')
                config.value =  result.json()
                config.save()
                print result.json()['name'] + ' successfully added to BOX'

            else:
                print 'Hospital not found in Alemhealth Platform'
        except requests.exceptions.Timeout as e:
            print str(e)
        except requests.exceptions.ConnectionError as e:
            print str(e)

        except IndexError:
            print 'Please enter Hospital GUID and client secret key'
        
