from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import requests
import logging
from alemhealthbox.models import *
logger = logging.getLogger('alemhealth')

class Command(BaseCommand):
    args = 'none'
    help = 'Sync Doctor and Providers'

    def handle(self, *args, **options):

        try:
            config_data = Config.objects.get(key='hospital')

        except Config.DoesNotExist:
            print 'Please run add_hospital command first'

        try:

            print 'fetching Doctors'
            hospital_id = config_data.value['id']
            url = settings.AH_SERVER + '/api/doctors?hospital=' + str(hospital_id) + '&page=-1&secret_key=' + settings.CLIENT_SECRET_KEY
            print  url
            logger.info('Fetching doctor url ' + url)
            result = requests.get(url)

            if result.status_code == 200:
                for doctor in result.json().get('doctors'):

                    doctor_obj, created = Doctor.objects.get_or_create(id=doctor['id'])
                    doctor_obj.name = doctor.get('name')
                    doctor_obj.specialty = doctor.get('specialty')
                    doctor_obj.email = doctor.get('email')
                    doctor_obj.phone = doctor.get('phone')
                    doctor_obj.city = doctor.get('city')
                    doctor_obj.country = doctor.get('country')
                    doctor_obj.save()

                    if created:
                        print doctor_obj.name + ' added '
                    else:
                        print doctor_obj.name + ' update'

            else:
                print 'Doctors not found in Alemhealth Platform'

            print 'fetching providers'

            url = settings.AH_SERVER + '/api/providers?page=-1&secret_key=' + settings.CLIENT_SECRET_KEY

            result = requests.get(url)

            if result.status_code == 200:
                for provider in result.json().get('providers'):

                    provider_obj, created = Provider.objects.get_or_create(id=provider['id'])
                    
                    provider_obj.name = provider.get('name')
                    provider_obj.save()

                    if created:
                        print provider_obj.name + ' added '
                    else:
                        print provider_obj.name + ' update'

            else:
                print 'Providers not found in Alemhealth Platform'

        except requests.exceptions.Timeout as e:
            print str(e)
        except requests.exceptions.ConnectionError as e:
            print str(e)

        