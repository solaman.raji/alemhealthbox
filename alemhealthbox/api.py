from django.views.generic import View
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth.models import Group

import logging
import re
import json


#from django_countries import countries

from alemhealthbox.utils import *
from alemhealthbox.decorators import *
#from alemhealth.emails import *

logger = logging.getLogger('alemhealth')


# get country list
def country_list(request):
    return dict_to_json(dict(countries), 200)


class UserResource(View):
    # def __init__(self):
    # self.required_fields = ["name"]

    def format(self, user):
        """
        Convert from an user object to a dict.
        """

        data = {
            'pk': user.pk,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'access_key': str(user.profile.access_key),
            'group': user.groups.all()[0].name

        }

        group_name_list = [a.name for a in user.groups.all()]

        if 'hospital' in group_name_list:
            data['hospital'] = {
                'id': user.hospitals.all()[0].pk,
                'name': user.hospitals.all()[0].name,
                'guid': str(user.hospitals.all()[0].guid)
            }

        return data

    def group_format(self, group):
        return {'name': group.name}


class UsersView(UserResource):
    # check email already exist or not
    @method_decorator(check_accesskey)
    def get(self, request):
        check_email = request.GET.get('check_email')
        email = request.GET.get('email')

        # just check email already exist or not
        if check_email:
            try:
                User.objects.get(username=email)
                return api_error_message("ResourceAlreadyExist", "User")
            except User.DoesNotExist:
                return dict_to_json({}, 200)

        request_type = request.GET.get('type')
        data = {}
        if request_type:
            users = User.objects.filter(groups__name__in=[request_type])
            data['users'] = [
                {'id': a.pk, 'username': a.username} for a in users]

        return dict_to_json(data, 201)


class UserView(UserResource):
    # authenticate a user
    def get(self, request, username):
        password = request.GET.get('password')
        if username != "" and password != "":

            is_email = re.search(r'\w+@\w+', username)

            try:
                if is_email:
                    chk_user = User.objects.get(email=username)
                    username = chk_user.username

                user = authenticate(username=username, password=password)
                if user is not None and user.is_active and user.groups.all():
                    logger.info(
                        user.email + " logging to alemhealth platform ")

                    data = self.format(user)
                    return dict_to_json(data, 201)

                return api_error_message("DoesNotExist", "User")
            except User.DoesNotExist:
                return api_error_message("DoesNotExist", "User")
        else:
            return api_error_message("DoesNotExist", "User")

    @method_decorator(check_accesskey)
    def put(self, request, username):

        try:
            json_dict = json.loads(request.body)
            user = User.objects.get(username=username)

            user.first_name = json_dict.get('first_name')
            user.last_name = json_dict.get('last_name')
            user.email = json_dict.get('email')

            if(json_dict.get('password')):
                user.set_password(json_dict.get('password'))

            user.save()

            logger.info(
                request.user.email +
                ' updated ' + user.email + ' profile')

            data = self.format(user)

        except User.DoesNotExist:
            return api_error_message("DoesNotExist", "User")
        except Exception as e:
            logger.error(str(e))

        return dict_to_json(data, 201)


class ConfigResource(View):
    
    def format(self, config):
        return config.value

        


class ConfigView(ConfigResource):
    def get(self, request, key):
        try:
            config_data = Config.objects.get(key=key)
        except Config.DoesNotExist:
             return api_error_message("DoesNotExist", "Config")

        data = self.format(config_data)

        return dict_to_json(data, 200)

        



class HospitalResource(View):

    def format(self, hospital):

        data = {
            'id': hospital.pk,
            'guid': str(hospital.guid),
            'user': self.user_format(hospital.user.all()[0]),
            'name': hospital.name,
            'hospital_type': hospital.hospital_type,
            'hospital_type_text': hospital.get_hospital_type_display(),
            'label': hospital.name,
            'address': hospital.address,
            'phone': hospital.phone,
            'general_email': hospital.general_email,
            'city': hospital.city,
            'country': str(hospital.country),
            'primary_name': hospital.primary_name,
            'primary_email': hospital.primary_email,
            'primary_physician_contact': hospital.primary_physician_contact,
            'doctors': [{
                            'name': a.name,
                            'guid': str(a.guid)
                        } for a in hospital.doctors.all()],
            'logo': hospital.logo_id if hospital.logo_id else None,
            'logo_file': str(hospital.logo.file) if hospital.logo else None,
            'operators': [self.user_format(operator.user) for operator in hospital.operators.all()]

        }

        return data

    def user_format(self, user):
        return {
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email
        }


class HospitalsView(HospitalResource):

    @method_decorator(check_accesskey)
    def post(self, request):
        json_dict = json.loads(request.body)
        user_dict = json_dict.get('user')

        email = user_dict.get('email')
        password = user_dict.get('password')

        try:
            user = User.objects.get(username=email)
            return api_error_message("ResourceAlreadyExist", "User")
        except User.DoesNotExist:

            try:
                user = User()
                user.first_name = user_dict.get('first_name')
                user.last_name = user_dict.get('last_name')
                user.username = user_dict.get('email')
                user.email = user_dict.get('email')
                user.set_password(password)
                user.save()

                g = Group.objects.get(name='hospital')
                g.user_set.add(user)

                logger.info(
                            request.user.email +
                            ' created a new hospital super user '
                            + user.email + ' profile'
                )

                json_dict = json.loads(request.body)
                hospital = Hospital()

                hospital.name = json_dict.get('name')
                hospital.hospital_type = json_dict.get('hospital_type')
                hospital.address = json_dict.get('address')
                hospital.phone = json_dict.get('phone')
                hospital.general_email = json_dict.get('general_email')
                hospital.city = json_dict.get('city')
                hospital.country = json_dict.get('country')
                hospital.primary_name = json_dict.get('primary_name')
                hospital.primary_email = json_dict.get('primary_email')
                hospital.primary_physician_contact = json_dict.get(
                    'primary_physician_contact')
                hospital.logo_id = json_dict.get('logo')

                hospital.save()
                hospital.user.add(user)

                logger.info(
                            request.user.email +
                            ' created a new hospital '
                            + hospital.name
                )

                if json_dict.get('sendEmail') == 'true':
                    user_registration_email(user, password)
            except Exception as e:
                logger.error(str(e))
                return

            data = self.format(hospital)

            return dict_to_json(data, 201)

    @method_decorator(check_accesskey)
    def get(self, request):

        post_per_page = 20
        page = request.GET.get('page')

        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        group_name_list = [a.name for a in request.user.groups.all()]

        hospitals = Hospital.objects.all().order_by('-created_at')

        if 'operator' in group_name_list:
            hospitals = request.user.operator.hospitals.all()

        count = hospitals.count()

        if page != -1 :
            hospitals = hospitals[offset:pagesize]
        data = {}

        data['meta'] = {
            "offset": offset,
            "pagesize": pagesize,
            "count": count
        }
        data['hospitals'] = [self.format(a) for a in hospitals]

        return dict_to_json(data, 200)

class HospitalView(HospitalResource):

    @method_decorator(check_accesskey)
    def get(self, request, guid):

        try:
            hospital = Hospital.objects.get(guid=guid)
        except Hospital.DoesNotExist:
            return api_error_message("DoesNotExist", "Hospital")

        data = self.format(hospital)

        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def put(self, request, guid):
        try:
            hospital = Hospital.objects.get(guid=guid)
        except Hospital.DoesNotExist:
            return api_error_message("DoesNotExist", "Hospital")

        json_dict = json.loads(request.body)
        user_dict = json_dict.get('user')

        password = user_dict.get('password')

        try:
            user = hospital.user.all()[0]
            user.first_name = user_dict.get('first_name')
            user.last_name = user_dict.get('last_name')
            user.username = user_dict.get('email')
            user.email = user_dict.get('email')

            if password:
                user.set_password(password)

            user.save()
        except Exception:
            return api_error_message("ResourceAlreadyExist", "User")

        try:
            hospital.name = json_dict.get('name')
            hospital.hospital_type = json_dict.get('hospital_type')
            #hospital.user.add(User.objects.get(pk=json_dict.get('user')))
            hospital.address = json_dict.get('address')
            hospital.phone = json_dict.get('phone')
            hospital.general_email = json_dict.get('general_email')
            hospital.city = json_dict.get('city')
            hospital.country = json_dict.get('country')
            hospital.primary_name = json_dict.get('primary_name')
            hospital.primary_email = json_dict.get('primary_email')
            hospital.primary_physician_contact = json_dict.get(
                'primary_physician_contact')

            # if hospital.logo_id :
            #     logo = hospital.logo
            #     logo.delete()
            #     hospital.logo_id = json_dict.get('logo')
            # else:

            if json_dict.get('logo'):
                hospital.logo_id = json_dict.get('logo')
            hospital.save()
        except Exception as e:
            logger.error(str(e))
            return api_error_message("IntegrityError", str(e))

        if json_dict.get('sendEmail') == 'true':
                if not password:
                    password = '*********'

                user_reset_email(user, password)

        logger.info(
                    request.user.email +
                    ' updated  hospital '
                    + hospital.name
                )

        data = self.format(hospital)

        return dict_to_json(data, 200)


class HospitalsLogoView(HospitalResource):
    @method_decorator(check_accesskey)
    def post(self, request):

        data = {}
        if request.FILES.get('file'):
            guid = request.POST.get('guid')
            if guid:
                hospital = Hospital.objects.get(guid=guid)
                if hospital.logo:
                    hospital.logo.file.delete()
                    new_file = hospital.logo

                else:
                    new_file = AhFile()
            else:
                new_file = AhFile()
            new_file.file = request.FILES.get('file')
            new_file.save()
            data['logo'] = new_file.id
            data['logo_file'] = str(new_file.file)

        return dict_to_json(data, 200)


class DoctorsResource(View):
    def format(self, doctor):
        #import pdb; pdb.set_trace()
        data = {
            'id' : doctor.pk,
            'guid': str(doctor.guid),
            'name': doctor.name,
        }
        return  data

    def user_format(self, user):
        if user:
            return {
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email
            }



class DoctorsView(DoctorsResource):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DoctorsView, self).dispatch(*args, **kwargs)

    #@method_decorator(csrf_exempt)
    @method_decorator(check_accesskey)
    def post(self, request):
        json_dict = json.loads(request.body)

        if json_dict.get('operator'):
            doctor = Doctor()
            doctor.name = json_dict.get('name')
            doctor.phone = json_dict.get('phone')
            doctor.save()

            data = self.format(doctor)

            return dict_to_json(data, 201)

        user_dict = json_dict.get('user')
        password = user_dict.get('password')

        try:
            user = User.objects.get(username=user_dict.get('email'))
            return api_error_message("ResourceAlreadyExist", "User")
        except User.DoesNotExist:
            user = User()
            user.first_name = user_dict.get('first_name')
            user.last_name = user_dict.get('last_name')
            user.username = user_dict.get('email')
            user.email = user_dict.get('email')
            user.set_password(password)
            user.save()

            g = Group.objects.get(name='doctor')
            g.user_set.add(user)

            logger.info(
                            request.user.email +
                            ' created a new doctor user '
                            + user.email + ' profile'
            )

            if json_dict.get('sendEmail') == 'true':
                user_registration_email(user, password)

            doctor = Doctor()
            doctor.name = user.first_name + ' ' + user.last_name
            doctor.specialty = json_dict.get('specialty')
            doctor.email = json_dict.get('email')
            doctor.phone = json_dict.get('phone')
            doctor.city = json_dict.get('city')
            doctor.country = json_dict.get('country')
            doctor.hospital_id = json_dict.get('hospital')
            doctor.radiologist = json_dict.get('radiologist')
            doctor.user = user

            if json_dict.get('signature'):
                doctor.signature_id = json_dict.get('signature')
            doctor.save()

        data = self.format(doctor)

        return dict_to_json(data, 201)

    @method_decorator(check_accesskey)
    def get(self, request):

        post_per_page = 20
        page = request.GET.get('page')
        hospital = request.GET.get('hospital')
        q = request.GET.get('q')
        dr_type = request.GET.get('type')
        radiologists = []
        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        # fetch all Doctors
        doctors = Doctor.objects.all().order_by('-created_at')

        # search Dr name for independent Dr and RO
        if q:
            doctors = doctors.filter(name__icontains=q)
            if dr_type == 'doctor':
                doctors = doctors.filter(radiologist=False)
            if dr_type == 'radiologist':
                doctors = doctors.filter(radiologist=True)

        # search independent Dr and RO for a hospital
        if hospital:
            temp_doctors = doctors
            doctors = doctors.filter(hospital_id=hospital, radiologist=False)
            radiologists = temp_doctors.filter(
                hospital_id=hospital, radiologist=True)

        count = doctors.count()

        if page != -1:
            doctors = doctors[offset:pagesize]
        data = {}

        data['meta'] = {
            "offset": offset,
            "pagesize": pagesize,
            "count": count
        }
        data['doctors'] = [self.format(a) for a in doctors]
        # fetch only radiologist type Dr
        if radiologists:
            data['radiologists'] = [self.format(a) for a in radiologists]

        return dict_to_json(data, 200)

class DoctorView(DoctorsResource):

    @method_decorator(check_accesskey)
    def get(self, request, guid):

        try:
            doctor = Doctor.objects.get(guid=guid)
        except Doctor.DoesNotExist:
            return api_error_message("DoesNotExist", "doctor")

        data = self.format(doctor)

        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def put(self, request, guid):

        try:
            doctor = Doctor.objects.get(guid=guid)
        except Doctor.DoesNotExist:
            return api_error_message("DoesNotExist", "doctor")

        json_dict = json.loads(request.body)

        user_dict = json_dict.get('user')
        password = user_dict.get('password')

        if doctor.user:
            try:
                #import pdb; pdb.set_trace()
                user = doctor.user
                user.first_name = user_dict.get('first_name')
                user.last_name = user_dict.get('last_name')
                user.username = user_dict.get('email')
                user.email = user_dict.get('email')

                if password:
                    user.set_password(password)

                user.save()

                if json_dict.get('sendEmail') == 'true':
                    if not password:
                        password = '*********'

                    user_reset_email(user, password)

            except Exception:
                return api_error_message("ResourceAlreadyExist", "User")

        else:
            # new user
            try:
                user = User.objects.get(username=user_dict.get('email'))
                return api_error_message("ResourceAlreadyExist", "User")
            except User.DoesNotExist:
                user = User()
                user.first_name = user_dict.get('first_name')
                user.last_name = user_dict.get('last_name')
                user.username = user_dict.get('email')
                user.email = user_dict.get('email')
                user.set_password(password)
                user.save()

                g = Group.objects.get(name='doctor')
                g.user_set.add(user)

                if json_dict.get('sendEmail') == 'true':
                    user_registration_email(user, password)

        doctor.user = user
        doctor.name = json_dict.get('name')
        doctor.specialty = json_dict.get('specialty')
        doctor.email = json_dict.get('email')
        doctor.phone = json_dict.get('phone')
        doctor.city = json_dict.get('city')
        doctor.country = json_dict.get('country')
        doctor.hospital_id = json_dict.get('hospital')
        doctor.radiologist = json_dict.get('radiologist')

        if json_dict.get('signature'):
            doctor.signature_id = json_dict.get('signature')

        doctor.save()

        data = self.format(doctor)

        logger.info(
                    request.user.email +
                    ' updated doctor '
                    + user.email + ' profile'
                )

        return dict_to_json(data, 200)


class DoctorSpecilitiesView(View):
    @method_decorator(check_accesskey)
    def get(self, request):

        post_per_page = 20
        page = request.GET.get('page')
        q = request.GET.get('q')

        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        doctor_specialies = DoctorSpeciality.objects.all().order_by('-pk')
        if q:
            doctor_specialies = doctor_specialies.filter(name__icontains=q)

        count = doctor_specialies.count()

        if page != -1 :
            doctor_specialies = doctor_specialies[offset:pagesize]
        data = {}

        data['meta'] = {
            "offset": offset,
            "pagesize": pagesize,
            "count": count
        }
        data['doctor_specialies'] = [{ 'name': a.name} for a in doctor_specialies]

        return dict_to_json(data, 200)

class DoctorSignatureView(DoctorsResource):
    @method_decorator(check_accesskey)
    def post(self, request):

        data = {}
        if request.FILES.get('file'):
            guid = request.POST.get('guid')
            if guid:
                doctor = Doctor.objects.get(guid=guid)
                if doctor.signature:
                    doctor.signature.file.delete()
                    new_file = doctor.signature
                else:
                    new_file = AhFile()

            else:
                new_file = AhFile()

            new_file.file = request.FILES.get('file')
            new_file.save()
            data['signature'] = new_file.id
            data['signature_file'] = str(new_file.file)

        return dict_to_json(data, 200)


class OperatorResource(View):

    def format(self, user):
        data = {
            'id': user.pk,
            'guid': str(user.profile.guid),
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email
        }
        try:
            if  user.operator.hospitals.all():
                data['hospitals'] = [{'id': hospital.pk, 'name': hospital.name}for hospital in user.operator.hospitals.all()]
        except Exception :
            pass
        return data


class OperatorsView(OperatorResource):

    @method_decorator(check_accesskey)
    def get(self, request):

        post_per_page = 20
        page = request.GET.get('page')

        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        # group = models.Group.objects.get(name='operator')

        operators = User.objects.filter(groups__name='operator')

        count = operators.count()

        if page != -1:
            operators = operators[offset:pagesize]
        data = {}

        data['meta'] = {
            "offset": offset,
            "pagesize": pagesize,
            "count": count
        }
        data['operators'] = [self.format(a) for a in operators]

        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def post(self, request):

        json_dict = json.loads(request.body)
        email = json_dict.get('email')
        password = json_dict.get('password')

        try:
            user = User.objects.get(username=email)
            return api_error_message("ResourceAlreadyExist", "User")
        except User.DoesNotExist:
            user = User()
            user.first_name = json_dict.get('first_name')
            user.last_name = json_dict.get('last_name')
            user.username = json_dict.get('email')
            user.email = json_dict.get('email')
            user.set_password(password)
            user.save()

            g = Group.objects.get(name='operator')
            g.user_set.add(user)

            logger.info(
                            request.user.email +
                            ' created a new operator user '
                            + user.email + ' profile'
            )

            try:
                operator = Operator()
                operator.user = user
                operator.save()

                for hospital in json_dict.get('hospitals'):
                    try:
                        hospital_obj = Hospital.objects.get(id=hospital['id'])
                        operator.hospitals.add(hospital_obj)
                    except Hospital.DoesNotExist:
                        pass

            except Exception as e:
                logger.error(str(e))
                user.delete()
                operator.delete()
                return api_error_message("IntegrityError", str(e))

            if json_dict.get('sendEmail') == 'true':
                user_registration_email(user, password)

            data = self.format(user)

            return dict_to_json(data, 200)


class OperatorView(OperatorResource):

    @method_decorator(check_accesskey)
    def get(self, request, guid):

        try:
            user = User.objects.get(profile__guid=guid)
        except User.DoesNotExist:
            return api_error_message("DoesNotExist", "User")

        data = self.format(user)

        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def put(self, request, guid):

        json_dict = json.loads(request.body)
        password = json_dict.get('password')

        try:
            user = User.objects.get(profile__guid=guid)
        except User.DoesNotExist:
            return api_error_message("DoesNotExist", "User")

        try:
            user.first_name = json_dict.get('first_name')
            user.last_name = json_dict.get('last_name')
            user.username = json_dict.get('email')
            user.email = json_dict.get('email')

            if password:
                user.set_password(password)

            user.save()
        except Exception as e:
            return api_error_message("ResourceAlreadyExist", "User")
        try:
            for hospital in user.operator.hospitals.all():
                user.operator.hospitals.remove(hospital)
        except Operator.DoesNotExist:
            operator = Operator()
            operator.user = user
            operator.save()

        #user.operator.hospitals.through.objects.all().delete()

        for hospital in json_dict.get('hospitals'):
                try:
                    hospital_obj = Hospital.objects.get(id=hospital['id'])
                    user.operator.hospitals.add(hospital_obj)
                except Hospital.DoesNotExist:
                    pass


        if json_dict.get('sendEmail') == 'true':
            if not password:
                    password = '*********'
            user_reset_email(user, password)

        data = self.format(user)

        return dict_to_json(data, 200)


class ProviderResource(View):

    def format(self, provider):

        data = {
            'id': provider.pk,
            'guid': str(provider.guid),
            'name': provider.name,
        }
        return data

    @method_decorator(check_accesskey)
    def get(self, request):

        post_per_page = 20
        page = request.GET.get('page')


        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        providers = Provider.objects.all().order_by('-created_at')

        count = providers.count()

        if page != -1 :
            providers = providers[offset:pagesize]
        data = {}

        data['meta'] = {
            "offset": offset,
            "pagesize": pagesize,
            "count": count
        }
        data['providers'] = [self.format(a) for a in providers]

        return dict_to_json(data, 200)



class ProvidersView(ProviderResource):

    @method_decorator(check_accesskey)
    def post(self, request):

        json_dict = json.loads(request.body)
        provider = Provider()

        provider.name = json_dict.get('name')
        provider.phone = json_dict.get('phone')
        provider.email = json_dict.get('email')
        provider.city = json_dict.get('city')
        provider.country = json_dict.get('country')

        provider.primary_name = json_dict.get('primary_name')
        provider.primary_email = json_dict.get('primary_email')
        provider.primary_phone = json_dict.get('primary_phone')

        provider.host = json_dict.get('host')
        provider.port = json_dict.get('port')
        provider.ae_title = json_dict.get('ae_title')

        provider.logo_id = json_dict.get('logo')

        provider.routine_turnaround = json_dict.get('routine_turnaround')
        provider.stat_turnaround = json_dict.get('stat_turnaround')

        provider.save()

        provider.user.add(User.objects.get(pk=json_dict.get('user')))
        provider.save()

        data = self.format(provider)

        return dict_to_json(data, 201)



class ProviderView(ProviderResource):

    @method_decorator(check_accesskey)
    def get(self, request, guid):

        try:
            provider = Provider.objects.get(guid=guid)
        except Provider.DoesNotExist:
            return api_error_message("DoesNotExist", "Provider")

        data = self.format(provider)

        return dict_to_json(data, 200)


    @method_decorator(check_accesskey)
    def put(self, request, guid):

        try:
            provider = Provider.objects.get(guid=guid)
        except Provider.DoesNotExist:
            return api_error_message("DoesNotExist", "Provider")

        json_dict = json.loads(request.body)

        provider.name = json_dict.get('name')
        provider.phone = json_dict.get('phone')
        provider.email = json_dict.get('email')
        provider.city = json_dict.get('city')
        provider.country = json_dict.get('country')

        provider.primary_name = json_dict.get('primary_name')
        provider.primary_email = json_dict.get('primary_email')
        provider.primary_phone = json_dict.get('primary_phone')

        provider.host = json_dict.get('host')
        provider.port = json_dict.get('port')
        provider.ae_title = json_dict.get('ae_title')

        provider.routine_turnaround = json_dict.get('routine_turnaround')
        provider.stat_turnaround = json_dict.get('stat_turnaround')

        provider.logo_id = json_dict.get('logo')

        provider.save()

        provider.user.add(User.objects.get(pk=json_dict.get('user')))
        provider.save()


        data = self.format(provider)

        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def delete(self, request, guid):

        try:
            provider = Provider.objects.get(guid=guid)
            provider.delete()
        except Provider.DoesNotExist:
            return api_error_message("DoesNotExist", 'Provider')

        return api_error_message("DeleteSuccessfully", "Provider")


class ProviderLogoView(ProviderResource):
    @method_decorator(check_accesskey)
    def post(self, request):

        data = {}
        if request.FILES.get('file'):
            #import pdb; pdb.set_trace()
            guid = request.POST.get('guid')
            if guid:
                provider = Provider.objects.get(guid=guid)
                if provider.logo:
                    provider.logo.file.delete()
                    new_file = provider.logo

                else:
                    new_file = AhFile()


            new_file.file = request.FILES.get('file')
            new_file.save()
            data['logo'] = new_file.id
            data['logo_file'] = str(new_file.file)


        return dict_to_json(data, 200)
