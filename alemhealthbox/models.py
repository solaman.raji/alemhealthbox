from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

from uuidfield import UUIDField
from alemhealthbox.jsonfield import JSONField

class UserProfile(models.Model):

    user = models.OneToOneField(User, related_name='profile')
    guid = UUIDField(auto=True)
    access_key = UUIDField(auto=True)

    class Meta:
        db_table = 'user_profiles'

    def __unicode__(self):
        return self.guid


# save add user profile while save user

def add_user_profile(sender, instance, **kwargs):
    try:
        if instance.id == 1:
            group, created = Group.objects.get_or_create(name='admin')
            group.user_set.add(instance)

        UserProfile.objects.get_or_create(user=instance)
    except Exception as e:
        pass

models.signals.post_save.connect(add_user_profile, sender=User)



class Config(models.Model):
    key = models.CharField(max_length=128, unique=True)
    value = JSONField(blank=True, null=True)

    class Meta:
        db_table = 'configs'

    def __unicode__(self):
        return self.key


class Patient(models.Model):

    blood_group_list = (
        ('O+', 'O+'),
        ('O-', 'O-'),
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),

    )

    user = models.OneToOneField(
        User, related_name='patients', null=True, blank=True)
    guid = UUIDField(auto=True)
    patient_id = models.CharField(max_length=128, blank=True, null=True)
    name = models.CharField(max_length=128, blank=True, null=True)
    gender = models.CharField(max_length=10, blank=True, null=True)
    age = models.CharField(max_length=10, blank=True, null=True)
    phone = models.CharField(max_length=128, blank=True, null=True)

    blood_group = models.CharField(
        max_length=5, choices=blood_group_list, blank=True, null=True)
    date_of_birth = models.CharField(max_length=128, blank=True, null=True)

    city = models.CharField(max_length=128, null=True, blank=True)
    country = models.CharField(max_length=128, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'patients'

    def __unicode__(self):
        return self.name

class Doctor(models.Model):

    guid = UUIDField(auto=True)
    user = models.OneToOneField(
        User, related_name='doctor', null=True, blank=True)

    name = models.CharField(max_length=128)
    specialty = models.CharField(max_length=128, null=True, blank=True)

    email = models.CharField(max_length=128, null=True, blank=True)
    phone = models.CharField(max_length=128, null=True, blank=True)
    city = models.CharField(max_length=128, null=True, blank=True)
    country = models.CharField(max_length=128, null=True, blank=True)

    radiologist = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'doctors'

    def __unicode__(self):
        return self.name

class Provider(models.Model):

    guid = UUIDField(auto=True)
    name = models.CharField(max_length=128)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'providers'

    def __unicode__(self):
        return self.name