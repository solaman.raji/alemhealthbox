from alemhealthbox.models import *
from alemhealthbox.utils import *
from django.conf import settings

def check_accesskey(f):
    def wrap(request, *args, **kwargs):

        # #this check the access key is valid or invalid
        if request.META.get('HTTP_AUTHENTICATION', ''):
            access_key = request.META.get('HTTP_AUTHENTICATION')
            try:
                user = UserProfile.objects.get(access_key=access_key)
                request.user = user.user
            except UserProfile.DoesNotExist:
                return api_error_message("InvalidAccessKey")
        elif request.GET.get('secret_key'):
            if request.GET.get('secret_key') != settings.CLIENT_SECRET_KEY:
                return api_error_message("InvalidAccessKey")

        else:
            return api_error_message("InvalidAccessKey")
        return f(request, *args, **kwargs)
    wrap.__doc__=f.__doc__
    wrap.__name__=f.__name__
    return wrap