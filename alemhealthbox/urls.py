from django.conf.urls import patterns, include, url

from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

	url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
       {'document_root': settings.MEDIA_ROOT}),

	url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
	       {'document_root': settings.STATIC_ROOT}),

    # Examples:
    url(r'^$', 'alemhealthbox.views.dashboard', name='dashboard'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    # api urls
    url(r'^', include('alemhealthbox.api_urls')),
    url(r'^', include('order.urls')),
    
)
